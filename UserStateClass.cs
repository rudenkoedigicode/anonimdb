﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimDB
{
    public class UserStateClass
    {
        public int ProgBarMain = 0;
        public int ProgBarSecond = 0;
        public string MessageText = "";

        public UserStateClass(string messageText, int progBarMain, int progBarSecond)
        {
            MessageText = messageText;
            ProgBarMain = progBarMain;
            ProgBarSecond = progBarSecond;
        }
    }
}
