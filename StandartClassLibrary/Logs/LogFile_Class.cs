﻿using System;
using System.Text;
using System.IO;

namespace StandartClassLibrary.Logs
{
    [Serializable()]
    public class LogFile_Class
    {
        public bool AllSaved { get; set; }
        public byte LifeLogFile { get; set; }
        public string LogFileName { get; set; }
        public bool SaveToFile { get; set; }
        public bool SaveToSQL { get; set; }
        public int SQLLogFileNumber { get; set; }
        public bool ShowLog { get; set; }
        public StreamWriter outputStream { get; set; }
    }
}
