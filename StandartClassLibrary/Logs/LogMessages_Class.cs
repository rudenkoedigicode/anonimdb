﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;

namespace StandartClassLibrary.Logs
{
    // <ComClass()> _
    // <Serializable()> _
    public class LogMessages
    {

        private ArrayList List = new ArrayList();

        private Logs_Class locParent;

        public LogMessages(Logs_Class newParent)
        {
            locParent = newParent;
        }

        public string ItemText(int index)
        {
                Logs_Class.LogStructure objMessage;
                if (((index >= 0)
                            || (index < Count())))
                {
                    objMessage = ((Logs_Class.LogStructure)(this.List[index]));
                    return objMessage.strMessage;
                }
                return null;
        }

        public string Writed(int index)
        {
                Logs_Class.LogStructure objMessage;
                if (((index >= 0)
                            || (index < Count())))
                {
                    objMessage = ((Logs_Class.LogStructure)(this.List[index]));
                    return objMessage.dtWrited.ToString(locParent.MessageFormatDateTime);
                }
                return "";
        }

        public Logs_Class.LogStructure this[Int32 index]
        {
            get
            {
                Logs_Class.LogStructure objMessage = new Logs_Class.LogStructure();
                if (((index >= 0)
                            || (index < Count())))
                {
                    objMessage = ((Logs_Class.LogStructure)(this.List[index]));
                    return objMessage;
                }
                return objMessage;
            }
            set
            {
                if (((index >= 0)
                            || (index < Count())))
                {
                    this.List[index] = value;
                }
            }
        }

        public void Add(Logs_Class.LogStructure Message)
        {
            List.Add(Message);
        }

        public void Remove(Int32 index)
        {
            List.RemoveAt(index);
        }

        public void Clear()
        {
            List.Clear();
        }

        public int Count()
        {
            return this.List.Count;
        }
    }
}
