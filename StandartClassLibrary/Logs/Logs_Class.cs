﻿using System;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Win32;
using System.IO;
using System.Threading;
using System.Data;
using System.Collections.Generic;
using StandartClassLibrary.ConnectDB;
using System.Linq;

namespace StandartClassLibrary.Logs
{
    public class Logs_Class  : IDisposable
    {
        #region Enums
        public enum logStates
        {
            Stoped = 0,
            Started,
            alreadyStarted,
        }

        #endregion

        #region Structures
        public struct LogStructure
        {
            public string strMessage;
            public DateTime dtWrited;
            public byte NumFileLog;
            public bool Saved;
        }

        public enum eLogStructure
        {
            Incorrect = 0,
            Message = 1,
            dtWrited,
            NumFileLog,
            LogItem,
        }

        public struct sLogProcess
        {
            public int ProcessID;
            public string Message;
            public long StartProcess;
            public byte LogFileNumber;
        }
        #endregion

        #region Variables

        private int locHistorySize = 100;
        private List<LogStructure> fullLog;
        protected List<LogFile_Class> locFiles = new List<LogFile_Class>();
        private DateTime locLastSavedDate;
        private string locPathLogs = "";
        private string locFullPathLogs = null;
        private string locSubFolded = "Logs";
        private byte locDefaultLifeLogFile = 7;
        protected System.Threading.Thread ThreadSaveLogs;
        protected System.Threading.Thread ThreadDeleteOldLogs;
        private bool LoopThread = true;
        protected logStates locState = logStates.Stoped;
        private string locMessageFormatDate = "dd/MM/yyyy";
        private string locMessageFormatTime = "HH:mm:ss.ffff";
        private string locMessageFormatDateTime = "dd/MM/yyyy HH:mm:ss.ffff";
        private string locFileFormatDate = "yyyy-MM-dd"; // HHmmss
        private System.Windows.Forms.Form locMDIParent;
        private System.IntPtr locLastShowHandle;
        private SQL_Class locSQL = null;
        public const string conLogSection = "Logs";
        public const byte LogError = 0;
        public const byte LogNone = 255;
        private IntPtr locLastErrorMessageHandle;
        private string locLogSession = DateTime.Now.ToString("HHmmss");

        private Dictionary<int, sLogProcess> arrProcess = new Dictionary<int, sLogProcess>();
        private byte defaultProcessLogFileNumber = 0;
        #endregion

        #region Constractor / Dispose
        //public Logs_Class()
        //{
        //    fullLog = new List<LogStructure>();
        //    locIni = null;
        //    PopupLogNumber = LogNone;
        //}

        public Logs_Class()
        {
            int intTMP;
            fullLog = new List<LogStructure>();

            PopupLogNumber = LogNone;
        }
        public void Dispose()
        {
            dispose(true);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.SuppressFinalize(this);
        }

        protected virtual void dispose(bool disposing)
        {
            if (disposing)
            {
                if (locSQL != null)
                {
                    locSQL.CloseConnection();
                }
                //  disposing
                this.Close();
                fullLog = null;
            }
        }
        
        ~Logs_Class()
        {
            if (locSQL != null)
            {
                locSQL.CloseConnection();
            }
            //  disposing
            this.Close();
            fullLog = null;
        }
        #endregion

        #region Properties
        public string Version
        {
            get
            {
                Type myType = this.GetType();
                string strReturn = (myType.Assembly.GetName().Version.Major + ("."
                            + (myType.Assembly.GetName().Version.Minor + ("." + myType.Assembly.GetName().Version.Build))));
                return strReturn;
            }
        }

        /// <summary>
        /// Формат даты для формирования имени логфайла
        /// </summary>
        public string FileFormatDate
        {
            get
            {
                return locFileFormatDate;
            }
            set
            {
                locFileFormatDate = value;
            }
        }

        /// <summary>
        /// Определяет суффикс после имени лог файла и перед датой лога. По умолчанию время запуска.
        /// </summary>
        public string LogSession
        {
            get
            {
                return locLogSession;
            }
            set
            {
                locLogSession = value;
            }
        }

        public List<LogStructure> GetFullLog
        {
            get
            {
                return fullLog;
            }
        }

        public List<LogFile_Class> LogFiles
        {
            get { return locFiles; }
        }

        /// <summary>
        /// Формат времени сообщения для записи в сам логфайл
        /// </summary>
        public string MessageFormatTime
        {
            get
            {
                return locMessageFormatTime;
            }
            set
            {
                locMessageFormatTime = value;
                MessageFormatDateTime = (locMessageFormatDate + (" " + locMessageFormatTime));
            }
        }

        /// <summary>
        /// Формат даты и времени сообщения для показа в окне логов
        /// </summary>
        public string MessageFormatDateTime
        {
            get
            {
                return locMessageFormatDateTime;
            }
            set
            {
                locMessageFormatDateTime = value;
            }
        }

        public int HistorySize
        {
            get
            {
                return locHistorySize;
            }
            set
            {
                locHistorySize = value;
            }
        }

        /// <summary>
        /// Кол-во дней сохранения логфайла на диске. Потом логфайл будет удален
        /// </summary>
        public byte DefaultLifeLogFile
        {
            get
            {
                return locDefaultLifeLogFile;
            }
            set
            {
                if ((value > 0))
                {
                    locDefaultLifeLogFile = value;
                }
            }
        }

        public logStates State
        {
            get
            {
                return locState;
            }
        }

        /// <summary>
        /// Имя подпапки для логфайлов. По умолчанию "Logs"
        /// </summary>
        public string SubFolder
        {
            get
            {
                return locSubFolded;
            }
            set
            {
                if ((value.Length > 0))
                {
                    locSubFolded = value;
                    locFullPathLogs = null;
                }
            }
        }

        /// <summary>
        /// Имя папки для размещения подпапок логов. По умолчанию это место запуска приложения.
        /// </summary>
        public string PathLogs
        {
            get
            {
                if (locFullPathLogs == null)
                {
                    try
                    {
                        if (locPathLogs.Length == 0)
                        {
                            locPathLogs = Environment.CurrentDirectory + Path.DirectorySeparatorChar;
                        }
                        else if (!locPathLogs.EndsWith(Path.DirectorySeparatorChar.ToString()))
                        {
                            locPathLogs += Path.DirectorySeparatorChar;
                        }

                        locFullPathLogs = locPathLogs + locSubFolded;
                        if (!Directory.Exists(locFullPathLogs))
                        {
                            Directory.CreateDirectory(locFullPathLogs);
                        }
                    }
                    catch (Exception ex)
                    {
                        ErrorInternal(ex);
                    }
                }
                if (!locFullPathLogs.EndsWith(Path.DirectorySeparatorChar.ToString()))
                    locFullPathLogs += Path.DirectorySeparatorChar;

                return locFullPathLogs;
            }
            set
            {
                locPathLogs = value;
                locFullPathLogs = null;
                //try
                //{
                //    if ((value.Length == 0))
                //    {
                //        locPathLogs = (Environment.CurrentDirectory
                //                    + (Path.DirectorySeparatorChar + locSubFolded));
                //    }
                //    else if ((value[(value.Length - 1)] == Path.DirectorySeparatorChar))
                //    {
                //        locPathLogs = (value + locSubFolded);
                //    }
                //    else
                //    {
                //        locPathLogs = (value + (Path.DirectorySeparatorChar + locSubFolded));
                //    }
                //    if (!Directory.Exists(locPathLogs))
                //    {
                //        Directory.CreateDirectory(locPathLogs);
                //    }
                //    locPathLogs += Path.DirectorySeparatorChar;
                //}
                //catch (Exception ex)
                //{
                //    ErrorInternal(ex);
                //}
            }
        }

        public System.Windows.Forms.Form MDIParent
        {
            get
            {
                return locMDIParent;
            }
            set
            {
                locMDIParent = value;
            }
        }

        public System.IntPtr LastShowHandle
        {
            get
            {
                return locLastShowHandle;
            }
            set
            {
                locLastShowHandle = value;
            }
        }

        public System.IntPtr LastErrorMessageHandle
        {
            get
            {
                return locLastErrorMessageHandle;
            }
            set
            {
                locLastErrorMessageHandle = value;
            }
        }

        public byte PopupLogNumber
        { get; set; }

        /// <summary>
        /// Номер лога по умолчанию если не указан в методе открытия логирования
        /// </summary>
        public byte DefaultProcessLogFileNumber
        {
            get
            {
                return defaultProcessLogFileNumber;
            }
            set
            {
                defaultProcessLogFileNumber = value;
            }
        }
        #endregion

        #region Events
        public delegate void ListHistoryChangedHandler(DateTime dtMessage, string strMessage, byte NumFileLog);
        public event ListHistoryChangedHandler ListHistoryChanged;
        #endregion

        #region Public methods
        public string GetPathFromCommanLine(string strCommandLine)
        {
            string strReturn = "";
            if ((strCommandLine.Length > 0))
            {
                strReturn = strCommandLine.Replace("\"", "");
                int i;
                i = (strReturn.Length - 1);
                while ((strReturn[i] != Path.DirectorySeparatorChar))
                {
                    i--;
                }
                strReturn = strReturn.Substring(0, i);
            }
            return strReturn;
        }
        /// <summary>
        /// Для нормального использования логов необходимо их открыть.
        /// </summary>
        /// <param name="ListLogFiles">Наименования файлов логов</param>
        public void Open(params string[] ListLogFiles)
        {
            try
            {
                if (locState == logStates.Stoped)
                {
                    if (ListLogFiles.GetUpperBound(0) >= 0)
                    {
                        for (byte i = 0; (i <= ListLogFiles.GetUpperBound(0)); i++)
                        {
                            AddNewLogFile(ListLogFiles[i]);
                        }
                    }

                    ThreadSaveLogs = new Thread(SaveUnsavedLogs);
                    ThreadSaveLogs.Name = "SaveUnsavedLogs";
                    ThreadSaveLogs.IsBackground = true; 
                    ThreadSaveLogs.Start();
                    locState = logStates.Started;
                }
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
        }

        /// <summary>
        /// Завершить работу лога.
        /// </summary>
        public void Close()
        {
            LoopThread = false;
            if (ThreadSaveLogs != null)
            {
                if ((ThreadSaveLogs.ThreadState == ThreadState.Running))
                {
                    ThreadSaveLogs.Join();
                }
            }
            locState = logStates.Stoped;
        }

        public int LogFileCount
        {
            get { return locFiles.Count; }
        }

        public LogFile_Class GetLogFile(int index)
        {
            return locFiles[index];
        }

        public void SetLogFile(int index, LogFile_Class NewLogFile)
        {
            if ((index > (locFiles.Count - 1)))
            {
                locFiles[index] = NewLogFile;
            }
        }

        /// <summary>
        /// Добовляет в список логов новый файл 
        /// </summary>
        /// <param name="LogFileName">Собственно имя лог файла</param>
        /// <param name="SaveToFile"></param>
        /// <param name="ShowLog"></param>
        public byte AddNewLogFile(string LogFileName)
        {
            byte newDemension = 0;
            LogFile_Class objLogFile = new LogFile_Class();
            const string conLogName = "LogFile";
            int conLogNameLength = conLogName.Length;
            string strTmp;
            byte intReturn = 255;
            try
            {
                if ((LogFileName.Length == 0))
                {
                    if ((locFiles.Count > 0))
                    {
                        for (byte i = 0; (i
                                    <= (locFiles.Count - 1)); i++)
                        {
                            if (((locFiles[i].LogFileName.Substring(0, conLogNameLength) == conLogName)
                                        && (locFiles[i].LogFileName.Substring((locFiles[i].LogFileName.Length - 1)) == "_")))
                            {
                                strTmp = locFiles[i].LogFileName.Substring(conLogNameLength, (locFiles[i].LogFileName.Length
                                                - (conLogNameLength - 1)));
                                if (Convert.ToByte(strTmp) >= newDemension)
                                {
                                    newDemension = (byte)(int.Parse(strTmp) + 1);
                                }
                            }
                        }
                    }
                    objLogFile.LogFileName = (conLogName
                                + (newDemension + "_"));
                }
                else
                {
                    objLogFile.LogFileName = LogFileName;
                }
                objLogFile.SaveToSQL = true;
                objLogFile.SaveToFile = true;
                objLogFile.ShowLog = true;
                objLogFile.LifeLogFile = DefaultLifeLogFile;
                objLogFile.AllSaved = true;

                locFiles.Add(objLogFile);
                intReturn = (byte)(locFiles.Count - 1);
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
            return intReturn;
        }

        public void RemoveLogFile(byte indexLog)
        {
            if (indexLog < locFiles.Count)
            {
                while (!locFiles[indexLog].AllSaved)
                {
                    Thread.Sleep(5);
                }
                locFiles.RemoveAt(indexLog);
            }
        }

        /// <summary>
        /// Запись исключения в логфайл с номером 0
        /// </summary>
        /// <param name="objException">Само исключение</param>
        public void WriteToLog(Exception objException)
        {
            string strTmp = GetMessageFromException(objException);
            //WriteToLog(strTmp, NumFiles.FileError);
            MultiWriteToLog(strTmp, LogError); //NumFiles.FileError);
        }

        /// <summary>
        /// Запись исключения в логфайл с определенным префиксом
        /// </summary>
        /// <param name="objException">Само исключение</param>
        /// <param name="strPrefix">Текстовая добавка спереди сообщения об исключении</param>
        public void WriteToLog(Exception objException, string strPrefix)
        {
            string strTmp = GetMessageFromException(objException);
            if (!string.IsNullOrEmpty(strPrefix))
            {
                strTmp = (strPrefix + (" - " + strTmp));
            }
            //WriteToLog(strTmp, NumFiles.FileError);
            MultiWriteToLog(strTmp, LogError);
        }

        public void WriteToLog(Exception objException, string strPrefix, string SuffixMessage)
        {
            string strTmp = GetMessageFromException(objException);
            if (!string.IsNullOrEmpty(strPrefix))
            {
                strTmp = (strPrefix + (" - " + strTmp));
            }
            if (!string.IsNullOrEmpty(SuffixMessage))
            {
                strTmp += (" - " + SuffixMessage);
            }
            //WriteToLog(strTmp, NumFiles.FileError);
            MultiWriteToLog(strTmp, LogError);
        }

        public void WriteToLog(Exception objException, string strPrefix, string SuffixMessage, byte NumFileLog1)
        {
            string strTmp = GetMessageFromException(objException);
            if (!string.IsNullOrEmpty(strPrefix))
            {
                strTmp = (strPrefix + (" - " + strTmp));
            }
            if (!string.IsNullOrEmpty(SuffixMessage))
            {
                strTmp += (" - " + SuffixMessage);
            }
            //WriteToLog(strTmp, NumFileLog1);
            MultiWriteToLog(strTmp, NumFileLog1);
        }

        public void WriteToLog(Exception objException, byte NumFileLog1)
        {
            string strTmp = GetMessageFromException(objException);
            MultiWriteToLog(strTmp, NumFileLog1);
        }

        public void WriteToLog(string strMessage)
        {
            //NumFiles[] arrNumFileLog = new NumFiles[0];
            try
            {
                if (string.IsNullOrEmpty(strMessage))
                {
                    strMessage = "Message empty 1";
                }
                //arrNumFileLog[0] = NumFiles.FileError;
                MultiWriteToLog(strMessage, LogError);
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
        }

        public void WriteToLog(string strMessage, byte NumFileLog1)
        {
            //NumFiles[] arrNumFileLog = new NumFiles[0];
            try
            {
                if (string.IsNullOrEmpty(strMessage))
                {
                    strMessage = "Message empty 1";
                }
                //object arrNumFileLog;
                //arrNumFileLog[0] = NumFileLog1;
                MultiWriteToLog(strMessage, NumFileLog1);
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
        }

        public void MultiWriteToLog(string newMessage, params byte[] NumFileLogs)
        {
            // Dim strTMP As String
            LogStructure OneMessage = new LogStructure();
            byte NumFileLog;
            try
            {
                if ((newMessage == null))
                {
                    newMessage = "Message empty 2";
                }
                if (((newMessage.Length > 0)
                            && (locState == logStates.Started)))
                {
                    OneMessage.dtWrited = DateTime.Now;
                    OneMessage.strMessage = newMessage;
                    if ((NumFileLogs.Length == 0))
                    {
                        NumFileLog = LogError;
                        OneMessage.NumFileLog = (byte)NumFileLog;
                        OneMessage.Saved = !locFiles[(int)NumFileLog].SaveToFile;
                        fullLog.Add(OneMessage);
                        locFiles[(int)NumFileLog].AllSaved = !locFiles[(int)NumFileLog].SaveToFile;
                        ListHistoryChanged(OneMessage.dtWrited, OneMessage.strMessage, (byte)NumFileLog);
                        //ListHistoryChanged(
                    }
                    else
                    {
                        for (Int16 i = 0; (i <= NumFileLogs.GetUpperBound(0)); i++)
                        {
                            if (((byte)NumFileLogs[i] > (locFiles.Count - 1)))
                            {
                                NumFileLog = 0;
                            }
                            else
                            {
                                NumFileLog = NumFileLogs[i];
                            }
                            OneMessage.NumFileLog = (byte)NumFileLog;
                            OneMessage.Saved = !locFiles[(int)NumFileLog].SaveToFile;
                            fullLog.Add(OneMessage);
                            locFiles[(int)NumFileLog].AllSaved = !locFiles[(int)NumFileLog].SaveToFile;
                            if (ListHistoryChanged != null)
                            {
                                ListHistoryChanged(OneMessage.dtWrited, OneMessage.strMessage, (byte)NumFileLog);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
        }

        /// <summary>
        /// Последнее сообщение лога
        /// </summary>
        /// <returns>Структура сообщения LogStructure</returns>
        public LogStructure GetLastMessage()
        {
            int LastMessageIndex = fullLog.Count;
            LogStructure LastMessage = new LogStructure();
            if ((LastMessageIndex > 0))
            {
                LastMessage = fullLog[(LastMessageIndex - 1)];
            }
            return LastMessage;
        }

        /// <summary>
        /// Вернуть последнее сообщение из конкретного лога
        /// </summary>
        /// <param name="NumFile">Номер Лога</param>
        /// <returns></returns>
        public LogStructure GetLastMessage(byte NumFile)
        {
            int LastMessageIndex = fullLog.Count;
            int index;
            LogStructure LastMessage = new LogStructure();
            if ((LastMessageIndex > 0))
            {
                for (index = LastMessageIndex - 1; index >= 0; index--)
                {
                    LastMessage = fullLog[index];
                    if (LastMessage.NumFileLog == NumFile)
                        break;
                }
            }
            return LastMessage;
        }

        public int ProcessLogStart(string Message, byte? DefaultLogNumber = null)
        {
            int ReturnValue = -1;

            if (arrProcess.Count == 0)
            {
                ReturnValue = 0;
            }
            else
            {
                ReturnValue = arrProcess.Max(r => r.Key) + 1;
            }

            sLogProcess newProcess = new sLogProcess();
            newProcess.ProcessID = ReturnValue;
            newProcess.Message = Message;
            newProcess.StartProcess = DateTime.Now.Ticks;
            newProcess.LogFileNumber = DefaultLogNumber != null ? (byte)DefaultLogNumber : DefaultProcessLogFileNumber;

            arrProcess.Add(ReturnValue, newProcess);

            WriteToLog(newProcess.Message + " started.", newProcess.LogFileNumber);

            return ReturnValue;
        }

        public void ProcessLogStop(string Message)
        {
            int ProcessID = arrProcess.Where(r => r.Value.Message == Message).Select(r => r.Key).FirstOrDefault();
            if (ProcessID > 0)
            {
                ProcessLogStop(ProcessID);
            }
        }

        public string ProcessLogStop(int ProcessID)
        {
            if (arrProcess.ContainsKey(ProcessID))
            {
                sLogProcess newProcess = arrProcess[ProcessID];
                TimeSpan Duration = new TimeSpan(DateTime.Now.Ticks - newProcess.StartProcess);
                string newMessage = newProcess.Message + string.Format(" finish. Duration: {0}:{1}:{2}.{3}", Duration.Hours, Duration.Minutes.ToString("00"), Duration.Seconds.ToString("00"), Duration.Milliseconds.ToString("000"));
                WriteToLog(newMessage, newProcess.LogFileNumber);
                arrProcess.Remove(ProcessID);
                return newMessage;
            }
            return "";
        }

        public string GetFileLogFullName(int NumFileLog)
        {
            return PathLogs + locFiles[NumFileLog].LogFileName +
                                DateTime.Now.ToString(FileFormatDate) +
                                (locLogSession.Length > 0 ? " " + locLogSession : "") +
                                ".log";
        }
        #endregion

        #region Private functions
        protected void SaveUnsavedLogs()
        {
            int DayOfMonth = DateTime.Now.Day;
            string strFilePath;
            StreamWriter outputStream;
            LogStructure CurrentMessage;
            byte NumFileLog;
            Int32 MessengeIndex;
            string strSQL;
            DataTable dtSender = null;
            DataRow drSender = null;
            System.Data.SqlClient.SqlBulkCopyColumnMappingCollection _SqlBulkCopyColumnMappingCollection = null;

            while (LoopThread || CheckUnsaved())
            {
                for (NumFileLog = 0; (NumFileLog <= (locFiles.Count - 1)); NumFileLog++)
                {
                    if ((locFiles[NumFileLog].SaveToFile
                                && !locFiles[NumFileLog].AllSaved))
                    {
                        try
                        {
                            strFilePath = GetFileLogFullName(NumFileLog);

                            //strFilePath = PathLogs + locFiles[NumFileLog].LogFileName + 
                            //    DateTime.Now.ToString(FileFormatDate) +
                            //    (locLogSession.Length > 0 ? " " + locLogSession : "") +
                            //    ".log";

                            #region Delete old files
                            if (locLastSavedDate < DateTime.Today)
                            {
                                locLastSavedDate = DateTime.Today;
                                if (ThreadDeleteOldLogs != null)
                                {
                                    if (((ThreadDeleteOldLogs.ThreadState == ThreadState.Aborted)
                                                && (ThreadDeleteOldLogs.ThreadState == ThreadState.Running)))
                                    {
                                        ThreadDeleteOldLogs.Join();
                                        ThreadDeleteOldLogs = null;
                                    }
                                }
                                ThreadDeleteOldLogs = new Thread(DeleteOldFiles);
                                ThreadDeleteOldLogs.IsBackground = true;
                                ThreadDeleteOldLogs.Start();
                            }
                            #endregion

                            #region Save to File
                            outputStream = new StreamWriter(strFilePath, true, System.Text.Encoding.Default);
                            if (fullLog != null)
                            {
                                lock (fullLog)
                                {
                                    for (MessengeIndex = 0; (MessengeIndex <= (fullLog.Count - 1)); MessengeIndex++)
                                    {
                                        CurrentMessage = fullLog[MessengeIndex];
                                        if ((!CurrentMessage.Saved
                                                    && (CurrentMessage.NumFileLog == NumFileLog)))
                                        {
                                            outputStream.WriteLine((CurrentMessage.dtWrited.ToString(MessageFormatTime) + ('\t' + CurrentMessage.strMessage)));
                                            CurrentMessage.Saved = true;
                                            fullLog[MessengeIndex] = CurrentMessage;
                                        }
                                    }
                                }
                            }
                            outputStream.Close();
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            ErrorInternal(ex);
                        }

                        outputStream = null;
                        locFiles[NumFileLog].AllSaved = true;
                        locLastSavedDate = DateTime.Now;
                    }
                }
                if (fullLog != null)
                {
                    while ((fullLog.Count > HistorySize))
                    {
                        try
                        {
                            fullLog.RemoveAt(1);
                        }
                        catch (Exception ex)
                        {
                            ErrorInternal(ex);
                        }
                    }
                }
                Thread.Sleep(500);
            }
            // MessageInternal("Logs '" & locPathLogs & "' Stoped")
        }

        protected void ErrorInternal(Exception ErrorMessage)
        {
            StreamWriter outputStream;
            string strMessage;
            string strFilePath = "D:\\Logs\\LogError1.log";
            strMessage = (DateTime.Now.ToString(MessageFormatDateTime) + (" - "
                        + (ErrorMessage.Message + ("\r\n" + ErrorMessage.StackTrace))));
            try
            {
                outputStream = new StreamWriter(strFilePath, true, System.Text.Encoding.Default);
                outputStream.WriteLine(strMessage);
                outputStream.Close();
            }
            catch
            {
            }
        }

        private bool CheckUnsaved()
        {
            for (Int16 NumFileLog = 0; (NumFileLog
                        <= (locFiles.Count - 1)); NumFileLog++)
            {
                if ((locFiles[NumFileLog].SaveToFile
                            && !locFiles[NumFileLog].AllSaved))
                {
                    return true;
                }
            }
            return false;
        }

        private void DeleteOldFiles()
        {
            FileInfo objFileInfo;
            string[] arrFileInfo;
            DateTime dtBefourDate;
            int d;
            //int j;
            int NumFileLog;
            try
            {
                for (NumFileLog = 0; (NumFileLog
                            <= (locFiles.Count - 1)); NumFileLog++)
                {
                    arrFileInfo = Directory.GetFiles(PathLogs, (locFiles[NumFileLog].LogFileName + "*.log"));
                    dtBefourDate = locLastSavedDate.AddDays((locFiles[NumFileLog].LifeLogFile * -1));
                    for (d = 0; (d <= arrFileInfo.GetUpperBound(0)); d++)
                    {
                        objFileInfo = new FileInfo(arrFileInfo[d]);

                        if (objFileInfo.CreationTime < dtBefourDate)
                        {
                            try
                            {
                                if (locSQL != null)
                                {
                                    string Session = objFileInfo.Name.Substring(objFileInfo.Name.IndexOf(' ') + 1, 6);
                                    string strSQL = "DELETE FROM [LogsTable] WHERE [LogFileNumber] = " + locFiles[NumFileLog].SQLLogFileNumber
                                        + " AND [Session] = '" + Session + "'";
                                    locSQL.ExecSQLDirect(strSQL);
                                }
                                objFileInfo.Delete();
                            }
                            catch
                            {
                            }
                            objFileInfo = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorInternal(ex);
            }
        }

        private string GetMessageFromException(Exception objException)
        {
            string strTmp = objException.Message + "\r\n" + objException.StackTrace;
            while (objException.InnerException != null)
            {
                objException = objException.InnerException;
                strTmp += "\r\nInnerException: " + objException.Message + "\r\n" + objException.StackTrace;
            }
            return strTmp;
        }
        #endregion
    }
}
