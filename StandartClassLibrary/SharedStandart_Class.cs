﻿using System;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;
using System.Windows.Forms;

namespace StandartClassLibrary
{
    public class Static_Class
    {
        /// <summary>
        /// Логический тип дополненный выражением unknown
        /// </summary>
        public enum eBoolean : byte
        {
            Unknown = 0,
            Yes,
            No,
        }


        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool IsWow64Process(
            [In] IntPtr hProcess,
            [Out] out bool wow64Process
        );

        /// <summary>
        /// Функция анализирует входное значение на предмет числа и дает следующее число по порядку
        /// </summary>
        /// <param name="strName">строковое значение имени</param>
        /// <returns>Новое значение имени</returns>
        public static string NewNameNumerator(string strName)
        {
            string strReturn = strName;
            int intTmp = strName.Length;
            int newNumber = 1;
            string strOnlyNumberts = "0123456789";
            if ((strName.Length > 0))
            {
                for (intTmp = strName.Length; (intTmp > 1); intTmp--)
                {
                    if (strOnlyNumberts.IndexOf(strName[intTmp - 1]) < 0)
                        break;
                }
                if ((intTmp < strName.Length))
                {
                    newNumber = (Convert.ToInt32(strName.Substring(intTmp)) + 1);
                    strReturn = strName.Substring(0, intTmp);
                }
            }
            return (strReturn + newNumber);
        }

        /// <summary>
        /// get file name from openFileDialog
        /// </summary>
        /// <param name="title"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public static string GetFileNameFromOFD(string title, string filter)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Title = title;// "Выберите файл который необходимо проверить";
            ofd.Filter = filter;// "Import/Export files (*.csv;*.xlsx;)|*.csv;*.xlsx;"; //full edition

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                return ofd.FileName;
            }
            return null;
        }

        public static void ShowMessageBox(string message)
        {
            MessageBox.Show(message);
        }

        public static string GetSaveFilePath()
        {
            SaveFileDialog sfd = new SaveFileDialog()
            {
                Title = "Сохранить файл как...",
                Filter = "Comma separated file (.csv)|*.csv",
                FilterIndex = 2
            };

            if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                if (File.Exists(sfd.FileName))
                {
                    try
                    {
                        File.Delete(sfd.FileName);
                    }
                    catch
                    {
                       throw new Exception("Нельзя перезаписать файл " + sfd.FileName + " т.к. он занят другим процессом!\r\nЗакройте все приложения использующие этот файл.");
                    }

                }
                return sfd.FileName;
            }
            return null;
        }



        /// <summary>
        /// определяет запущено ли еще один раз это приложение
        /// </summary>
        /// <returns>true если запущенно</returns>
        public static bool InstanceExists()
        {
            Process CurrentProcess = Process.GetCurrentProcess();
            Process[] Processes;
            string _NameProcess = CurrentProcess.ProcessName;
            Processes = Process.GetProcessesByName(_NameProcess);
            foreach (Process Proc in Processes)
            {
                if ((Proc.Id != CurrentProcess.Id))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Дополняет строку до определенной длины одним символом слева или справа
        /// </summary>
        /// <param name="Source">Исходная строка</param>
        /// <param name="intLength">необходимая длина строки</param>
        /// <param name="FillChar">Символ заполнения</param>
        /// <param name="FillLeft">заполнять слева или справа</param>
        /// <returns>переделанная строка</returns>
        public static string FillString(string Source, Int16 intLength, char FillChar, bool FillLeft = true)
        {
            string strAdd = "";
            if ((Source.Length < intLength))
            {
                strAdd = new string(FillChar, (intLength - Source.Length));
                if (FillLeft)
                {
                    Source = (strAdd + Source);
                }
                else
                {
                    Source += strAdd;
                }
            }
            return Source;
        }

        //public static object ConvertStringToEnum(string strSource, System.Type enumType) {
        //    byte objEnum = 0;
        //    try {
        //        objEnum = Enum.Parse(typeof(enumType), strSource, true);
        //    }
        //    catch (Exception ex) {
        //    }
        //    return objEnum;
        //}

        /// <summary>
        /// Предпологалось зашифровать строку
        /// </summary>
        /// <param name="sText">что шифруем</param>
        /// <returns>зашифрованная строка</returns>
        public static string StringEncryption(string sText)
        {
            System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
            byte[] sByteArr = encoding.GetBytes(sText);
            string rString = "";
            string xs;
            for (int idx = 0; (idx <= sByteArr.GetUpperBound(0)); idx++)
            {
                //xs = Convert.ToString(sByteArr[idx], 8);
                //xs = Convert.ToString(int.Parse(xs), 16).PadLeft(2, '0');
                xs = Convert.ToString(sByteArr[idx], 16).PadLeft(2, '0');
                xs = xs[1].ToString() + xs[0].ToString();
                rString = (rString + xs);
            }
            return rString;
        }

        /// <summary>
        /// Расшифровать строку
        /// </summary>
        /// <param name="sText">зашифрованная строка</param>
        /// <returns>Разшифрованная строка</returns>
        public static string StringDecoding(string sText)
        {
            string rString = "";
            string xs;
            int xsi;
            try
            {
                for (int idx = 1; (idx <= sText.Length); idx = (idx + 2))
                {
                    xs = sText.Substring((idx - 1), 2);
                    xs = xs[1].ToString() + xs[0].ToString();
                    //xs = Convert.ToString(int.Parse("&h" + xs));
                    xsi = int.Parse(xs, System.Globalization.NumberStyles.HexNumber);
                    //xs = Convert.ToString(Convert.ToInt16(xs, 8));
                    //rString = (rString + ((char)(Convert.ToInt32(xs))));
                    rString += (char)(xsi);
                }
            }
            catch
            {
                rString = "";
            }
            return rString;
        }

        /// <summary>
        /// Вычисляет делитель для значения. К примеру у нас есть число 56 000 000, а мы хотим не более 1 000 000, тогда GetDevider(56000000, 1000000) вернет 100.
        /// </summary>
        /// <param name="MaxValue"></param>
        /// <param name="MaxWantedValue"></param>
        /// <returns></returns>
        public static long GetDevider(long MaxValue, long MaxWantedValue = 10000)
        {
            double conDevider = 1;
            long lngReturn;
            int intZeroCount;

            for (intZeroCount = 1; intZeroCount <= 10; intZeroCount++)
            {
                conDevider = Math.Pow((double)10, (double)intZeroCount);
                if (conDevider * MaxWantedValue > MaxValue)
                    break;
            }
            lngReturn = Convert.ToInt64(conDevider);
            return lngReturn;
        }

        const string NotValidSymbol = "/\\',:*?<>|~!@#$%^&=`.\"";

        /// <summary>
        /// Возвращает имя папки в формате символов понятных для ftp & CarID
        /// </summary>
        /// <param name="strSourceFolder">Исходное имя папки</param>
        /// <returns></returns>
        public static string RenameFolderToOurFormat(string strSourceFolder)
        {
            int i;
            string strTemp;
            string NotValidSymbol_Folders = "®™";

            strTemp = strSourceFolder.Trim();

            for (i = 0; i < NotValidSymbol_Folders.Length; i++)
            {
                if (strTemp.IndexOf(NotValidSymbol_Folders.Substring(i, 1)) > -1)
                {
                    strTemp = strTemp.Replace(NotValidSymbol_Folders.Substring(i, 1), "");
                }
            }


            while (strTemp.IndexOf("  ") > -1)
            {
                strTemp = strTemp.Replace("  ", " ");
            }
            
            while (strTemp.EndsWith("."))
            {
                strTemp = strTemp.Substring(0, strTemp.Length - 1);
            }
            
            for (i = 0; i < NotValidSymbol.Length; i++)
            {
                if (strTemp.IndexOf(NotValidSymbol.Substring(i, 1)) > -1)
                {
                    strTemp = strTemp.Replace(NotValidSymbol.Substring(i, 1), "-");
                }
            }
            
            while (strTemp.IndexOf("--") > -1)
            {
                strTemp = strTemp.Replace("--", "-");
            }
            
            strTemp = strTemp.Replace(" -", "-");
            strTemp = strTemp.Replace("- ", "-");
            strTemp = strTemp.Replace(" ", "-");
            return strTemp.ToLower();
        }

        /// <summary>
        /// Возвращает имя файла в формате символов понятных для ftp & CarID
        /// </summary>
        /// <param name="strSourceFileName">Исходное имя файла</param>
        /// <returns></returns>
        public static string RenameFileToOurFormat(string strSourceFileName)
        {
            int i;
            string strFileExt;
            string strTemp;
            if (strSourceFileName.IndexOf(".") > -1)
            {
                strFileExt = strSourceFileName.Trim().ToLower();
                strFileExt = strFileExt.Substring(strFileExt.LastIndexOf(".") + 1);
                strFileExt = strFileExt.Replace("jpeg", "jpg");
                strTemp = strSourceFileName.Substring(0, (strSourceFileName.Length - strFileExt.Length - 1));
                strTemp = strTemp.Trim().ToLower();
                
                while (strTemp.IndexOf("  ") > -1)
                {
                    strTemp = strTemp.Replace("  ", " ");
                }
                
                for (i = 0; i < NotValidSymbol.Length; i++)
                {
                    if (strTemp.IndexOf(NotValidSymbol.Substring(i, 1)) > -1)
                    {
                        strTemp = strTemp.Replace(NotValidSymbol.Substring(i, 1), "-");
                    }
                }
                
                while (strTemp.IndexOf("--") > -1)
                {
                    strTemp = strTemp.Replace("--", "-");
                }

                return strTemp + "." + strFileExt;
            }
            else
            {
                return strSourceFileName;
            }
        }

        private static bool InternalCheckIsWow64()
        {
            if ((Environment.OSVersion.Version.Major == 5 && Environment.OSVersion.Version.Minor >= 1) ||
                Environment.OSVersion.Version.Major >= 6)
            {
                using (Process p = Process.GetCurrentProcess())
                {
                    bool retVal;
                    if (!IsWow64Process(p.Handle, out retVal))
                    {
                        return false;
                    }
                    return retVal;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Определяет 64х разрядную систему
        /// </summary>
        /// <returns>true если таки 64х разрядная система</returns>
        public static bool is64BitOperatingSystem()
        {
            bool is64BitProcess = (IntPtr.Size == 8);

            return is64BitProcess || InternalCheckIsWow64();
        }

        /// <summary>
        /// Добавляет к числу его наименование в нужном падеже. К примеру 1 ручка, 2 ручки, 5 ручек.
        /// </summary>
        /// <param name="OriginalCount">Само число</param>
        /// <param name="ForOne">Наименование для 1, 21, 31 и т.д.</param>
        /// <param name="ForTwo">Наименование для 2, 3, 4 и т.д.</param>
        /// <param name="ForFive">Наименование для остальных. если null то используется ForTwo</param>
        /// <param name="WithNumber">Определяет будет ли число возвращено вместе с наименованием</param>
        /// <returns></returns>
        public static string CountWithName(int OriginalCount, string ForOne, string ForTwo, string ForFive = null, bool WithNumber = true)
        {
            string strReturn = (WithNumber ? OriginalCount.ToString() : "");
            string additinal = (WithNumber ? " " : "");

            if ((strReturn.EndsWith("2") && !strReturn.EndsWith("12")) 
                || (strReturn.EndsWith("3") && !strReturn.EndsWith("13")) 
                || (strReturn.EndsWith("4")) && !strReturn.EndsWith("14"))
            {
                strReturn += additinal + ForTwo;
            }
            else if (strReturn.EndsWith("1") && !strReturn.EndsWith("11"))
            {
                strReturn += additinal + ForOne;
            }
            else
            {
                strReturn += additinal + (ForFive == null ? ForTwo : ForFive);
            }
            return strReturn;
        }


        /// <summary>
        /// реализует логику вызова ф-и ShowWinFormMessage сборки ProjectTool.exe класса ModalMessageBox
        /// </summary>
        /// <param name="message">текст сообщения</param>
        /// <param name="caption">заголовок окна</param>
        /// <param name="messageBoxButtons">кнопки окна</param>
        /// <param name="messageBoxIcon">иконка окна</param>
        public static System.Windows.Forms.DialogResult ShowModalMessageBox(object message, object caption, object messageBoxButtons, object messageBoxIcon)
        {
            System.Windows.Forms.DialogResult dialogResult = System.Windows.Forms.DialogResult.None;

            try
            {
                Assembly projectToolAssembly = Assembly.LoadFrom("ProjectTool.exe");
                Type modalMessageBoxType = projectToolAssembly.GetType("ProjectTool.ModalMessageBox");
                var method = (modalMessageBoxType != null) ? modalMessageBoxType.GetMethod("ShowWinFormMessage", BindingFlags.Public | BindingFlags.Static) : null;
                dialogResult = (System.Windows.Forms.DialogResult)method.Invoke(null, new object[] { message, caption, messageBoxButtons, messageBoxIcon });

            }
            catch (FileNotFoundException fnfEx)
            {
                dialogResult = System.Windows.Forms.MessageBox.Show((string)message, (string)caption, (System.Windows.Forms.MessageBoxButtons)messageBoxButtons, (System.Windows.Forms.MessageBoxIcon)messageBoxIcon);
               // dbgWizard.glbLog.WriteToLog(fnfEx, "ProjectTool.exe not found");
            }
            catch (Exception ex)
            {
                throw;              
            }

            return dialogResult;
        }
    }
}
