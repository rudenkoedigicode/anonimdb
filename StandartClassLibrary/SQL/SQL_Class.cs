﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using StandartClassLibrary.Logs;
using System.Data;
using System.Linq;
using StandartClassLibrary;

namespace StandartClassLibrary.ConnectDB
{
    public partial class SQL_Class : IDisposable //, ICloneable
    {
        #region Variables
        private SqlConnectionStringBuilder _SqlConnectionBuilder = new SqlConnectionStringBuilder();
        private string strOriginDatabaseName = "";
        private Logs_Class _Logs;
        private byte _LogSqlScriptsNumber;

        private byte _ProfileID = 0;
        private Dictionary<string, string> _arrAliases = new Dictionary<string, string>();
        //private Hashtable _arrAliases = new Hashtable();
        private string[] _arrDataTables;
        private bool _arrAliasesHaveDifferents = false;

        //private SqlConnection _SQLConnection;

        private Static_Class.eBoolean _LogSqlScripts = Static_Class.eBoolean.Unknown;

        private SqlConnection _DirectSQLConnection;

        private bool _PasswordEncription = false;
        private bool _ShowOptions = false;

        private string conNone = "(none)";

        private TimeSpan _DifferentTime;
        //private int _DifferentSecond;
        private bool _DifferentSecondNotChecked = true;

        private double _LastCommandExecutionTime = -1;
        private eAsyncOperations _AsyncOperation = eAsyncOperations.DoNothing;
        private static string[] strSeparator = new string[] { " / " };
        private int _CurrentPermission = -2;
        private int MaxReconnectCount = 3;
        #endregion

        #region Enum
        public enum eSQLErrors : byte
        {
            Unknown = 0,
            Ok,
            DoNotDifineINI,
            IncorrectServer,
            IncorrectDatabase
        }

        /// <summary>
        /// Как пишем логфайл.
        /// </summary>
        public enum eWriteToLog : byte
        {
            ByDefault = 0,
            AlwaysWrite,
            NeverWrite
        }

        /// <summary>
        /// Определяет работу асинхроноого режима
        /// </summary>
        public enum eAsyncOperations : byte
        {
            DoNothing = 0,
            Running,
            DoCancel
        }

        public enum eUseHash : byte
        {
            ByDefault = 0,
            SendDirect,
        }

        /// <summary>
        /// Определяет как выделено в DataGridView
        /// </summary>
        public enum eSelectedObjects
        {
            None = 0,
            Cell,
            Rows,
            Columns
        }

        /// <summary>
        /// Определяет, какие поля из таблицы будут возвращены в списке в функции GetListFieldsFromTable
        /// </summary>
        public enum eGetFields
        {
            All = 0,
            NonEmpty,
            OnlyEmpty
        }

        public enum OptionCopyTable
        {
            None = 0,
            CreateDestination = 1,
            DropDestination = 2,
            CreateIndexes = 4,
            BeQuietIfSourceNotExists = 8,
        }
        #endregion

        #region Structures
        public struct DataGridViewSavedPositions
        {
            //private List<string> _LastSelected;
            //public List<string> LastSelected
            //{
            //    get
            //    {
            //        if (_LastSelected == null) _LastSelected = new List<string>();
            //        return _LastSelected;
            //    }
            //    set
            //    {
            //        _LastSelected = value;
            //    }
            //}
            //private int _firstDisplayedScrollingColumnIndex;
            //public int firstDisplayedScrollingColumnIndex
            //{
            //    get
            //    {
            //        if (_firstDisplayedScrollingColumnIndex == null) _firstDisplayedScrollingColumnIndex = -1;
            //        return _firstDisplayedScrollingColumnIndex;
            //    }
            //    set
            //    {
            //        _firstDisplayedScrollingColumnIndex = value;
            //    }
            //}
            //private int _firstDisplayedScrollingRowIndex;
            //public int firstDisplayedScrollingRowIndex
            //{
            //    get
            //    {
            //        if (_firstDisplayedScrollingRowIndex == null) _firstDisplayedScrollingRowIndex = -1;
            //        return _firstDisplayedScrollingRowIndex;
            //    }
            //    set
            //    {
            //        _firstDisplayedScrollingRowIndex = value;
            //    }
            //}
            public List<string> LastSelected;
            public int firstDisplayedScrollingColumnIndex;
            public int firstDisplayedScrollingRowIndex;

            public string strSortedColumn;
            public System.ComponentModel.ListSortDirection objSortOrder;
            public eSelectedObjects GridSelectedObjects;

            //public DataGridViewSavedPositions()
            //{
            //    LastSelected = new List<string>();
            //    firstDisplayedScrollingColumnIndex = -1;
            //    firstDisplayedScrollingRowIndex = -1;
            //    strSortedColumn = "";
            //    objSortOrder = default(System.ComponentModel.ListSortDirection);
            //    GridSelectedObjects = eSelectedObjects.None;
            //}
        }
        #endregion

        #region Events
        //public event SQLServerChangedEventHandler SQLServerChanged;
        //public delegate void SQLServerChangedEventHandler();
        //public event SQLDatabaseChangedEventHandler SQLDatabaseChanged;
        //public delegate void SQLDatabaseChangedEventHandler();
        //public event SQLUserIDChangedEventHandler SQLUserIDChanged;
        //public delegate void SQLUserIDChangedEventHandler();
        //public event SQLPasswordChangedEventHandler SQLPasswordChanged;
        //public delegate void SQLPasswordChangedEventHandler();
        //public event SQLConnectTimeoutChangedEventHandler SQLConnectTimeoutChanged;
        //public delegate void SQLConnectTimeoutChangedEventHandler();
        public event ConnectedChangedEventHandler ConnectedChanged;
        public delegate void ConnectedChangedEventHandler();

        public event CurrentDatadaseChangedEventHandler CurrentDatadaseChanged;
        public delegate void CurrentDatadaseChangedEventHandler(string newDataBase);
        #endregion

        #region new / dispose
        ///<summary>
        ///Создание функционального класса для работы с SQL
        ///</summary>
        ///<param name="ptrLog">Лог объект. Должен быть уже открытым</param>
        ///<param name="LogSqlScriptsNumber">Номер лога в лог объекте.</param>
        ///<param name="ApplicationName">ApplicationID в описании SQL Connection. Если не определен, то устанавливается "StandSQL"</param>
        ///<param name="ptrINI">INI объект. Допускается Nothing</param>
        ///<param name="arrAliases">Массив псевдонимов баз данных для сохранения в INI файле. Допускается Nothing</param>

        public SQL_Class(Logs_Class ptrLog, byte LogSqlScriptsNumber, string ApplicationName)
        {
            WithExceptions = false;
            RunAsynchronous = true;
            LogRetry = false;
            CountRetry = 3;
            _Logs = ptrLog;
            if (LogSqlScriptsNumber > 0)
            {
                _LogSqlScriptsNumber = LogSqlScriptsNumber;
            }
            else
            {
                LogSqlScripts = Static_Class.eBoolean.No;
            }

            try
            {
                //_SQLConnection = new SqlConnection();
                _DirectSQLConnection = new SqlConnection();

                if (ApplicationName.Length == 0)
                {
                    ApplicationName = "StandSQL";
                }
                _SqlConnectionBuilder.ApplicationName = ApplicationName;
                _SqlConnectionBuilder.MultipleActiveResultSets = true;
                _SqlConnectionBuilder.CurrentLanguage = "English";

            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
            }
        }

        ///<summary>
        ///закрытие всех SQL соединений и очистка пула.
        ///</summary>
        public void CloseConnection()
        {
            _DifferentSecondNotChecked = true;
            if (_DirectSQLConnection.State == ConnectionState.Open)
            {
                try
                {
                    _DirectSQLConnection.Close();
                }
                catch { }
            }
            _DirectSQLConnection.Dispose();
            //SqlConnection.ClearAllPools();

        }

        public void Dispose()
        {
            CloseConnection();
            //GC.Collect();
            //GC.WaitForPendingFinalizers();
            GC.SuppressFinalize(this);
        }

        ~SQL_Class()
        {
            CloseConnection();
        }
        #endregion

        #region Common Property
        /// <summary>
        /// Логобъект определенный при подключении. Используется в формах подключения.
        /// </summary>
        public Logs_Class glbLogs
        {
            get { return _Logs; }
        }

        /// <summary>
        ///Определяет показывать ли опции настройки SQL подключения в форме.
        /// </summary>
        public bool ShowOptions
        {
            get { return _ShowOptions; }
            set { _ShowOptions = value; }
        }

        /// <summary>
        ///Возвращает текущее подключение.
        /// </summary>
        public SqlConnectionStringBuilder GetSqlConnectionBuilder
        {
            get
            {
                return _SqlConnectionBuilder;
            }
        }

        /// <summary>
        /// Возвращает текущее SQL соединение класса
        /// </summary>
        public SqlConnection DirectSQLConnection
        {
            get
            {
                return _DirectSQLConnection;
            }
        }

        /// <summary>
        /// Записывать все выполняемые SQL запросы в логфайл или нет.
        /// </summary>
        public Static_Class.eBoolean LogSqlScripts
        {
            get
            {
                if (_LogSqlScripts == Static_Class.eBoolean.Unknown)
                {
                        if (_LogSqlScriptsNumber > 0)
                        {
                            _LogSqlScripts = Static_Class.eBoolean.Yes;
                        }
                        else
                        {
                            _LogSqlScripts = Static_Class.eBoolean.No;
                        }
                }

                return _LogSqlScripts;
            }
            set
            {
                _LogSqlScripts = value;
            }
        }

        /// <summary>
        /// Номер логфайла в логобъекте.
        /// </summary>
        public byte LogSqlScriptsNumber
        {
            get { return _LogSqlScriptsNumber; }
        }

        /// <summary>
        /// Выполнено ли соединение.
        /// </summary>
        public bool Connected
        {
            get
            {
                if (_DirectSQLConnection != null)
                    return (_DirectSQLConnection.State == ConnectionState.Open);
                else
                    return false;
            }
        }

        //public bool PasswordEncription
        //{
        //    get { return _PasswordEncription; }
        //    set
        //    {
        //        if (_PasswordEncription != value)
        //        {
        //            _SqlConnectionBuilder.Password = "";
        //        }
        //        _PasswordEncription = value;
        //    }
        //}

        /// <summary>
        /// При возникновении Exception вызывать Exception для класса или делать всё в тихую.
        /// </summary>
        public bool WithExceptions { get; set; }

        /// <summary>
        /// Пишет в лог когда происходит пересоединение.
        /// </summary>
        public bool LogRetry { get; set; }

        /// <summary>
        /// Определяет как запускать Execute, getScalar, getDatatable.
        /// по умолчанию true
        /// </summary>
        public bool RunAsynchronous { get; set; }

        /// <summary>
        /// Возвращает или задает работу в асинхронном режиме. Возможна отмена выполнения асинхронной команды.
        /// </summary>
        public eAsyncOperations AsyncOperation
        {
            get
            {
                return _AsyncOperation;
            }
            set
            {
                _AsyncOperation = value;
            }
        }

        ///<summary>
        ///Временя выполнения последней операции.
        ///</summary>
        public double LastCommandExecutionTime
        {
            get { return _LastCommandExecutionTime; }
        }

        ///<summary>
        ///Значение при достижение которого выполненный скрипт будет записан в лог со временем его выполнения. Милисекунды.
        ///</summary>
        public long MaximumLastCommandExecutionTime { get; set; }

        /// <summary>
        /// Кол-во повторений команды (GetScalar, ExecuteDirect, GetDataTable, GetDataRow)
        /// Если 0 то повторения отключены и ошибка генерируется сразу.
        /// Если -1 то повторения будут до реально выполненной команды.
        /// </summary>
        public int CountRetry { get; set; }
        #endregion

        #region SQL Property
        public string SQLConnectionString
        {
            get { return _SqlConnectionBuilder.ConnectionString; }
            set
            {
                SqlConnectionStringBuilder objSqlConnectionBuilder = new SqlConnectionStringBuilder(value);
                _SqlConnectionBuilder = objSqlConnectionBuilder;
            }
        }

        public string SQLServer
        {
            get { return _SqlConnectionBuilder.DataSource; }
            set
            {
                _SqlConnectionBuilder.DataSource = value;
            }
        }

        public string SQLDatabase
        {
            get { return _SqlConnectionBuilder.InitialCatalog; }
            set
            {
                _SqlConnectionBuilder.InitialCatalog = value;
            }
        }

        public string SQLUserID
        {
            get { return _SqlConnectionBuilder.UserID; }
            set
            {
                _SqlConnectionBuilder.UserID = value;
            }
        }

        public string SQLPassword
        {
            get { return _SqlConnectionBuilder.Password; }
            set
            {
                _SqlConnectionBuilder.Password = value;
            }
        }

        public int SQLConnectTimeout
        {
            get { return _SqlConnectionBuilder.ConnectTimeout; }
            set
            {
                _SqlConnectionBuilder.ConnectTimeout = value;
            }
        }

        public int SQLMaxReconnectCount
        {
            get { return MaxReconnectCount; }
            set
            {
                MaxReconnectCount = value;
            }
        }
        #endregion

        #region Connection Function
        /// <summary>
        /// Построить ConnectionString на основании параметров
        /// </summary>
        /// <param name="strServer">SQL Server</param>
        /// <param name="strDatabase">Database</param>
        /// <param name="strUserID">User ID</param>
        /// <param name="strPassword">Password</param>
        /// <returns></returns>
        public string BuildConnectionString(string strServer, string strDatabase, string strUserID, string strPassword)
        {
            string newConnectionString = "";

            if (strServer.Length != 0 & strDatabase.Length != 0)
            {
                if (_SqlConnectionBuilder.ApplicationName.Length > 0)
                {
                    newConnectionString = "Application Name=" + _SqlConnectionBuilder.ApplicationName + ";";
                }
                newConnectionString = "Data Source=" + strServer + ";Initial Catalog=" + strDatabase + ";";

                if (strUserID.Length != 0)
                {
                    newConnectionString += "User ID=" + strUserID + ";Password=" + strPassword + ";Persist Security Info=True;";
                }
                else
                {
                    newConnectionString += "Integrated Security=SSPI;";
                }
                return newConnectionString;
            }
            else
            {
                return "";
            }
        }

        /// <summary>
        /// Проверить ConnectionString на предмет соединение с сервером.
        /// </summary>
        /// <param name="strConnectionString">ConnectionString</param>
        /// <returns>Возвращает true если соединение произошло успешно</returns>
        public bool CheckSQLConnection(string strConnectionString)
        {

            if (strConnectionString.Length > 0)
            {
                try
                {
                    SqlConnection objSQLConnection = new SqlConnection(strConnectionString);

                    objSQLConnection.ConnectionString = strConnectionString;
                    objSQLConnection.Open();

                    objSQLConnection.Close();

                }
                catch (Exception ex)
                {
                    _Logs.WriteToLog("CheckSQLConnection" + "\r\n" + ex.Message + "\r\n" + strConnectionString, _LogSqlScriptsNumber);
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Генерирует информационную строку подключения.
        /// </summary>
        /// <returns>Возвращает сейчас только имя сервера.</returns>
        public string GetDataBaseToolStripStatusLabel(bool WithDataBaseName = false)
        {
            string strReturn = "";
            try
            {
                strReturn = SQLServer;
                if (WithDataBaseName)
                    strReturn += SQLDatabase;
            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
            }
            return strReturn;
        }

        ///<summary>
        ///Подключиться к SQL серверу
        ///</summary>
        /// <param name="ReloadFromINI">Загрузить все настройки сервера с INI файла с текущего профайла</param>
        /// <param name="ReConnect"> Если соединение було уже открыто то закрывается и открывается заново. Иначе только открывается если было закрыто.</param>
        /// <returns>Возвращает true если подключение благополучно прошло.</returns>
        public bool ConnectToSQL(bool ReConnect = true)
        {
            // использовать SQLMaxReconnectCount (по умолчанию = 3)
            long StartTicks = System.DateTime.Now.Ticks;
            bool blnReturn = false;
            eSQLErrors objSQLErrors = eSQLErrors.Ok;

            switch (objSQLErrors)
            {
                case eSQLErrors.DoNotDifineINI:
                    //_Logs.WriteToLog("Ini file not defined.", _LogSqlScriptsNumber);

                    break;
                case eSQLErrors.IncorrectServer:
                    _Logs.WriteToLog("Incorrect Server", _LogSqlScriptsNumber);

                    break;
                case eSQLErrors.IncorrectDatabase:
                    _Logs.WriteToLog("Incorrect Database", _LogSqlScriptsNumber);

                    break;
                case eSQLErrors.Ok:
                    if (_DirectSQLConnection.State == ConnectionState.Open && ReConnect)
                    {
                        if (_AsyncOperation == eAsyncOperations.Running)
                        {
                            _AsyncOperation = eAsyncOperations.DoCancel;
                            while (_AsyncOperation == eAsyncOperations.DoNothing)
                                Application.DoEvents();
                        }
                        CloseConnection();
                    }

                    try
                    {
                        if (_DirectSQLConnection.State == ConnectionState.Closed)
                        {
                            _DirectSQLConnection.ConnectionString = _SqlConnectionBuilder.ConnectionString;
                            _DirectSQLConnection.Open();
                        }

                        if (ConnectedChanged != null)
                        {
                            ConnectedChanged();
                        }

                        blnReturn = true;
                    }
                    catch (Exception ex)
                    {
                        _Logs.WriteToLog("BuildConnectionString.Open Connection" + "\r\n" + ex.Message + "\r\n" + _DirectSQLConnection.ConnectionString, _LogSqlScriptsNumber);
                    }

                    break;
            }
            CalculateExecutionTime(StartTicks, "Open connection");

            return blnReturn;
        }
        #endregion

    }
}
