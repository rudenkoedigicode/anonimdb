﻿using System;

namespace StandartClassLibrary.ConnectDB
{
    public partial class SQL_Class : IDisposable //, ICloneable
    {
        private void CalculateExecutionTime(long StartTicks, string strSQL)
        {
            TimeSpan tsExecutionTime = new TimeSpan(DateTime.Now.Ticks - StartTicks);
            _LastCommandExecutionTime = tsExecutionTime.TotalMilliseconds;
            if (MaximumLastCommandExecutionTime > 0)
            {
                if (_LastCommandExecutionTime > MaximumLastCommandExecutionTime)
                {
                    string strTime = "";
                    if (tsExecutionTime.Hours > 0)
                    {
                        strTime = tsExecutionTime.Hours + ":";
                    }
                    if (tsExecutionTime.TotalMinutes > 60)
                    {
                        strTime += tsExecutionTime.Minutes.ToString("00") + ":";
                    }
                    strTime += tsExecutionTime.Seconds.ToString("00") + ":" + tsExecutionTime.Milliseconds.ToString("00");
                    _Logs.WriteToLog("Время выполнения скрипта: " + strTime + " - " + strSQL, _LogSqlScriptsNumber);
                }
            }
        }
    }
}
