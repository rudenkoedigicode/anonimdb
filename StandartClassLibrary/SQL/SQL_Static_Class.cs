﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;
using StandartClassLibrary.Logs;
using System.Data;
using System.Linq;
using StandartClassLibrary;

namespace StandartClassLibrary.ConnectDB
{
    public partial class SQL_Class : IDisposable //, ICloneable
    {
        public static string GetSQLCommand(DataRow SourceRow, bool blnInsert = false)
        {
            string strSQL = "";
            string strSQL1 = "";
            string strSQL2 = "";
            string strTmp = null;
            DataColumn objColumn = null;
            Int16 i = default(Int16);
            //Try

            if (blnInsert)
            {
                for (i = 0; i <= SourceRow.Table.Columns.Count - 1; i++)
                {
                    objColumn = SourceRow.Table.Columns[i];
                    if (!objColumn.AutoIncrement)
                    {
                        strTmp = GetFieldValue(SourceRow[i], objColumn);
                        if ((strTmp == "Null" & objColumn.AllowDBNull) | (strTmp != "Null"))
                        {
                            if (strSQL1.Length > 0)
                            {
                                strSQL1 += ", ";
                                strSQL2 += ", ";
                            }
                            strSQL1 += objColumn.ColumnName;
                            strSQL2 += strTmp;
                        }
                    }
                }
                strSQL = "Insert into " + SourceRow.Table.TableName + "(" + strSQL1 + ") Values(" + strSQL2 + ")";
            }
            else
            {
                for (i = 0; i <= SourceRow.Table.Columns.Count - 1; i++)
                {
                    objColumn = SourceRow.Table.Columns[i];
                    if (objColumn.AutoIncrement)
                    {
                        strSQL2 = " Where " + objColumn.ColumnName + "=" + GetFieldValue(SourceRow[i], objColumn);
                    }
                    else
                    {
                        strTmp = GetFieldValue(SourceRow[i], objColumn);
                        if (strTmp.Length > 0)
                        {
                            if (strSQL1.Length > 0)
                            {
                                strSQL1 += ", ";
                            }
                            strSQL1 += objColumn.ColumnName + " = " + strTmp;
                        }
                    }
                }
                strSQL = "Update " + SourceRow.Table.TableName + " SET " + strSQL1 + strSQL2;
            }
            //Catch ex As Exception
            //_Logs.WriteToLog(ex, _LogSqlScriptsNumber)
            //End Try
            return strSQL;
        }

        /// <summary>
        /// Возвращает строковое значение из поля для операторов INSERT, UPDATE и выражений WHERE. Причем кавычки или NULL проставляются в зависимости от поля.
        /// </summary>
        /// <param name="objField">Поле взятое из DataRow</param>
        /// <param name="objColumn">DataColumn этой Data</param>
        /// <returns>Строковое значение поля</returns>
        public static string GetFieldValue(object objField, DataColumn objColumn)
        {
            bool AllowDBNull = objColumn.AllowDBNull;
            string sScript = "";
            const string strNULL = "Null";

            //Try
            if (objField == null)
            {
                if (AllowDBNull)
                {
                    return strNULL;
                }
            }
            if (Convert.IsDBNull(objField))
            {
                return strNULL;
            }
            switch (objColumn.DataType.Name)
            {
                //objField.GetType.Name
                case "Single":
                case "Double":
                case "Decimal":
                    sScript = Convert.ToString(objField).Replace(",", ".");
                    break;
                case "Boolean":
                    if (Convert.ToBoolean(objField))
                    {
                        sScript = "1";
                    }
                    else
                    {
                        sScript = "0";
                    }
                    break;
                case "Int16":
                case "Int32":
                case "Int64":
                case "Byte":
                case "UInt16":
                case "UInt32":
                case "UInt64":
                    sScript = Convert.ToString(objField);
                    break;
                case "DateTime":
                    sScript = "'" + ((DateTime)objField).ToString("yyyy/MM/dd HH:mm:ss") + "'";
                    break;
                case "String":
                    if (AllowDBNull & Convert.ToString(objField).Length == 0)
                    {
                        sScript = strNULL;
                    }
                    else
                    {
                        sScript = "N'" + objField.ToString().Replace("'", "''") + "'";
                    }
                    break;
                case "DBNull":
                    if (AllowDBNull)
                    {
                        sScript = strNULL;
                    }
                    break;
                default:
                    sScript = objField.ToString();
                    break;
            }
            //Catch ex As Exception
            //_Logs.WriteToLog(ex, _LogSqlScriptsNumber)
            //End Try
            //==========================
            //        Case adLongVarBinary
            //            sScript = "0x"
            //            Dim arrTmpByte
            //            Dim arrTmpStr() As String
            //            arrTmpByte = objField.Value
            //            ReDim arrTmpStr(UBound(arrTmpByte))
            //            For i As Int32 = 0 To UBound(arrTmpByte)
            //                arrTmpStr(i) = Hex(arrTmpByte(i))
            //            Next
            //            sScript = Join(arrTmpStr, "")
            //        Case Else
            //            Call MsgBox("Unkown Data type '" & objField.Name _
            //                & "'", vbCritical, "Program terminated")
            //            End
            //    End Select
            //End If
            return sScript;
        }

        /// <summary>
        /// Возвращает строковое значение из поля для операторов INSERT, UPDATE и выражений WHERE. Причем кавычки или NULL проставляются в зависимости от поля.
        /// </summary>
        /// <param name="objField">Поле взятое из DataRow</param>
        /// <param name="AllowDBNull">Разрешает значение NULL если объект реально NULL. Иначе будет 0 для числовых и '' для строковых</param>
        /// <returns>Строковое значение поля</returns>
        public static string GetFieldValue(object objField, bool AllowDBNull)
        {
            string sScript = "";
            const string strNULL = "NULL";

            //Try
            if (objField == null)
            {
                if (AllowDBNull)
                {
                    return strNULL;
                }
            }
            if (Convert.IsDBNull(objField))
            {
                return strNULL;
            }
            switch (objField.GetType().Name)
            {
                //objField.GetType.Name
                case "Single":
                case "Double":
                case "Decimal":
                    sScript = Convert.ToString(objField).Replace(",", ".");
                    break;
                case "Boolean":
                    if (Convert.ToBoolean(objField))
                    {
                        sScript = "1";
                    }
                    else
                    {
                        sScript = "0";
                    }
                    break;
                case "Int16":
                case "Int32":
                case "Int64":
                case "Byte":
                case "UInt16":
                case "UInt32":
                case "UInt64":
                    sScript = Convert.ToString(objField);
                    break;
                case "DateTime":
                    sScript = "'" + ((DateTime)objField).ToString("yyyy/MM/dd HH:mm:ss") + "'";
                    break;
                case "String":
                    if (AllowDBNull && objField == DBNull.Value)
                    {
                        sScript = strNULL;
                    }
                    else
                    {
                        sScript = "'" + objField.ToString().Replace("'", "''") + "'";
                    }
                    break;
                case "DBNull":
                    if (AllowDBNull)
                    {
                        sScript = strNULL;
                    }
                    break;
                default:
                    sScript = objField.ToString();
                    break;
            }
            //Catch ex As Exception
            //_Logs.WriteToLog(ex, _LogSqlScriptsNumber)
            //End Try
            //==========================
            //        Case adLongVarBinary
            //            sScript = "0x"
            //            Dim arrTmpByte
            //            Dim arrTmpStr() As String
            //            arrTmpByte = objField.Value
            //            ReDim arrTmpStr(UBound(arrTmpByte))
            //            For i As Int32 = 0 To UBound(arrTmpByte)
            //                arrTmpStr(i) = Hex(arrTmpByte(i))
            //            Next
            //            sScript = Join(arrTmpStr, "")
            //        Case Else
            //            Call MsgBox("Unkown Data type '" & objField.Name _
            //                & "'", vbCritical, "Program terminated")
            //            End
            //    End Select
            //End If
            return sScript;
        }

        public static string IsFieldNull(object objSource, string IfNullValue)
        {
            if (Convert.IsDBNull(objSource))
            {
                return IfNullValue;
            }
            else
            {
                return objSource.ToString();
            }
        }

        /// <summary>
        /// Разделяет имя таблицы и базы данных. Возвращает true если разделение произошло.
        /// </summary>
        /// <param name="FullTableName">Полное имя таблицы</param>
        /// <param name="DataBaseName">База данных</param>
        /// <param name="TableName">Имя таблицы</param>
        /// <returns>Возвращает true если разделение произошло.</returns>
        public static bool SplitFullTableName(string FullTableName, ref string DataBaseName, ref string TableName)
        {
            bool WasFullName = false;
            //string DataBaseName = "";
            //string TableName = "";
            string[] arrComponents = FullTableName.Split('.');
            if (arrComponents.GetUpperBound(0) == 2)
            {
                DataBaseName = arrComponents[0].Replace("[", "");
                DataBaseName = DataBaseName.Replace("]", "");
                WasFullName = true;
            }

            TableName = arrComponents[arrComponents.GetUpperBound(0)].Replace("[", "");
            TableName = TableName.Replace("]", "");

            return WasFullName;
        }

        public static bool ExistField(SqlDataReader rsSource, string FindField)
        {
            Int16 i = default(Int16);
            for (i = 0; i <= rsSource.FieldCount - 1; i++)
            {
                if (rsSource.GetName(i) == FindField)
                {
                    return true;
                }
            }
            return false;
        }

        public static bool ExistField(DataRow rsSource, string FindField)
        {
            try
            {
                object ColumnExist = rsSource[FindField];
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool ExistField(DataTable rsSource, string FindField)
        {
            try
            {
                object ColumnExist = rsSource.Columns[FindField];
                if (ColumnExist == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Возвращает имя таблицы из запроса, если заранее известно что таблица одна
        /// </summary>
        /// <param name="strSQL">Сам запрос</param>
        /// <returns>Имя таблицы</returns>
        public static string GetTableName(string strSQL)
        {
            string strReturn = "";
            int intFrom = strSQL.ToUpper().IndexOf(" FROM ");
            int intLenght = 0;

            if (intFrom > -1)
            {
                intFrom += 6;
                intLenght = strSQL.IndexOf(" ", intFrom);
                if (intLenght > 0)
                {
                    strReturn = strSQL.Substring(intFrom, intLenght - intFrom);
                }
                else
                {
                    if (intFrom < strSQL.Length)
                    {
                        strReturn = strSQL.Substring(intFrom);
                    }
                }
            }
            return strReturn.Replace(";", "");
        }

        /// <summary>
        /// Возвращает имя таблиц из запроса. Не работает при сложных запросах и подзапросах.
        /// </summary>
        /// <param name="strSQL">Сам запрос</param>
        /// <returns>Массив имён таблиц</returns>
        public static string[] GetTablesName(string strSQL)
        {
            string strTmp = null;
            string[] strReturn = null;
            strReturn = new string[1];
            strReturn[0] = GetTableName(strSQL);

            int intFrom = strSQL.ToUpper().IndexOf(" FROM ");
            int intLenght = 0;

            intFrom = strSQL.ToUpper().IndexOf(" JOIN ");
            while (intFrom > -1)
            {
                intFrom += 6;
                intLenght = strSQL.IndexOf(" ", intFrom);
                if (intLenght > 0)
                {
                    strTmp = strSQL.Substring(intFrom, intLenght - intFrom);
                    Array.Resize(ref strReturn, strReturn.GetUpperBound(0) + 2);
                    strReturn[strReturn.GetUpperBound(0)] = strTmp;
                }

                intFrom = strSQL.ToUpper().IndexOf(" JOIN ", intFrom);
            }
            return strReturn;
        }

        public static string GetDataColumnSQLtype(DataColumn _Column)
        {
            string strReturn = "";
            switch (_Column.DataType.Name)
            {
                case "Boolean":
                    strReturn = "BIT";
                    break;
                case "Byte":
                    strReturn = "TINYINT";
                    break;
                case "Int16":
                    strReturn = "SMALLINT";
                    break;
                case "Int32":
                    strReturn = "INT";
                    break;
                case "Int64":
                    strReturn = "BIGINT";
                    break;
                case "Single":
                    strReturn = "FLOAT";
                    break;
                case "Double":
                    strReturn = "REAL";
                    break;
                case "Char":
                    strReturn = "NCHAR(1)";
                    break;
                case "String":
                    strReturn = "NVARCHAR(" + ((_Column.MaxLength == -1 || _Column.MaxLength > 4000) ? "MAX" : _Column.MaxLength.ToString()) + ")";
                    break;
                case "DateTime":
                    strReturn = "DATETIME";
                    break;
                default:
                    break;
            }
            return strReturn;
        }

        public static bool SmartBoolConvert(object Source)
        {
            bool ReturnValue = false;

            if (Source.GetType().Name == "Boolean")
                ReturnValue = Convert.ToBoolean(Source);
            else if (Source.GetType().Name == "String")
            {
                ReturnValue = (Source.ToString() == "1" || Source.ToString().ToUpper() == "TRUE");
            }

            return ReturnValue;
        }

        public static string GetStringFromList(List<string> SourceList, int intExcludeField = -1, string strAliase = "")
        {
            string strReturn = "";
            if (strAliase.Length > 0)
            {
                if (!strAliase.EndsWith("."))
                {
                    strAliase += ".";
                }
            }
            for (int iField = 0; iField <= SourceList.Count - 1; iField++)
            {
                if (iField != intExcludeField)
                {
                    if (strReturn.Length > 0)
                    {
                        strReturn += ", ";
                    }
                    strReturn += strAliase + "[" + SourceList[iField] + "]";
                }
            }
            return strReturn;
        }

        public static string GetStringFromList(List<string> SourceList, string[] arrExcludeFields, string strAliase = "")
        {
            string strReturn = "";
            if (strAliase.Length > 0)
            {
                if (!strAliase.EndsWith("."))
                {
                    strAliase += ".";
                }
            }
            for (int iField = 0; iField <= SourceList.Count - 1; iField++)
            {
                if (!arrExcludeFields.Contains(SourceList[iField]))
                {
                    if (strReturn.Length > 0)
                    {
                        strReturn += ", ";
                    }
                    strReturn += strAliase + "[" + SourceList[iField] + "]";
                }
            }
            return strReturn;
        }

        public static string GetStringFromList(List<string> SourceList, List<string> arrExcludeFields, string strAliase = "")
        {
            string strReturn = "";
            if (strAliase.Length > 0)
            {
                if (!strAliase.EndsWith("."))
                {
                    strAliase += ".";
                }
            }
            for (int iField = 0; iField <= SourceList.Count - 1; iField++)
            {
                if (!arrExcludeFields.Contains(SourceList[iField]))
                {
                    if (strReturn.Length > 0)
                    {
                        strReturn += ", ";
                    }
                    strReturn += strAliase + "[" + SourceList[iField] + "]";
                }
            }
            return strReturn;
        }
    }
}
