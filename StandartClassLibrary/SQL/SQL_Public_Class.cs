﻿using System;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;
using System.Threading;

namespace StandartClassLibrary.ConnectDB
{
    public partial class SQL_Class : IDisposable
    {
        /// <summary>
        /// Проверить существует ли имя поля в таблице.
        /// </summary>
        /// <param name="TableName">Имя таблицы в которой ищем поле.</param>
        /// <param name="FieldName">Имя поля для проверки.</param>
        /// <param name="DataBaseName">Имя базы данных в которой находится таблица</param>
        /// <returns>Возвращает true если поле существует в таблице, иначе false</returns>
        public bool CheckFieldInTable(string TableName, string FieldName, string DataBaseName = "")
        {
            string _TableName = "";
            SplitFullTableName(TableName, ref DataBaseName, ref _TableName);

            if (DataBaseName.Length > 0)
            {
                if (!DataBaseName.EndsWith("."))
                    DataBaseName += ".";
            }
            //string strSQL = "SELECT COUNT(*) AS ColumnCount " +
            //    "FROM " + DataBaseName + "dbo.syscolumns c  WITH (NOLOCK) INNER JOIN " + DataBaseName + "dbo.sysobjects so WITH (NOLOCK) ON so.id=c.id  " +
            //    "WHERE so.name = '" + TableName + "' AND c.name = '" + FieldName + "'";

            string strSQL = "SELECT COUNT(*) AS ColumnCount " +
                "FROM " + DataBaseName + "INFORMATION_SCHEMA.COLUMNS WHERE table_name = '" + _TableName + "' AND column_name = '" + FieldName + "'";
            int intValue = GetScalarValue(strSQL, 0);
            return (intValue > 0);
        }

        /// <summary>
        /// Возвращает true если такая таблица существует.
        /// </summary>
        /// <param name="FullTableName">Имя таблицы. Допускается полное имя с базой данных</param>
        /// <param name="DataBaseName">Имя базы данных в которой ищется таблица. Если указано полное имя то игнорируется</param>
        public bool TableExist(string FullTableName, string DataBaseName = "")
        {
            string TableName = "";
            SplitFullTableName(FullTableName, ref DataBaseName, ref TableName);

            if (DataBaseName.Length > 0)
            {
                if (!DataBaseName.EndsWith("."))
                    DataBaseName += ".";
            }
            string strSQLCheckExistTable = "SELECT Count(*) as RecCount FROM " + DataBaseName + "sys.objects WHERE object_id = OBJECT_ID(N'" + DataBaseName + "[dbo].[" + TableName + "]') AND type in (N'U')";
            return (GetScalarValue(strSQLCheckExistTable, 0) > 0);
        }

        /// <summary>
        /// Сменить текущую базу данных. Проверяется так же на псевдонимы базы если они были заданны.
        /// </summary>
        /// <param name="newDatabaseName">Имя базы данных</param>
        /// <returns>Если смена базы произошла успешно то true</returns>
        public bool ChangeDatabase(string newDatabaseName)
        {
            if (Connected)
            {
                if (newDatabaseName.Length > 0) // && _SqlConnectionBuilder.InitialCatalog != newDatabaseName)
                {
                    strOriginDatabaseName = newDatabaseName;
                    if (_arrAliases.Count == 0)
                    {
                        _DirectSQLConnection.ChangeDatabase(newDatabaseName);

                        _SqlConnectionBuilder.InitialCatalog = newDatabaseName;

                        if (CurrentDatadaseChanged != null)
                            CurrentDatadaseChanged(newDatabaseName);
                        return true;
                    }
                    else
                        if (_arrAliases.ContainsKey(newDatabaseName))
                    {
                        string strAliase = (string)_arrAliases[newDatabaseName];
                        //if (SQLDatabase.ToUpper() != strAliase.ToUpper())
                        //{
                        int counter = 0;
                        while (true)
                        {
                            try
                            {
                                _DirectSQLConnection.ChangeDatabase(strAliase);

                                _SqlConnectionBuilder.InitialCatalog = strAliase;

                                if (CurrentDatadaseChanged != null)
                                    CurrentDatadaseChanged(strAliase);
                                return true;
                            }
                            catch (Exception ex)
                            {
                                counter++;
                                if (counter > CountRetry)
                                {

                                    _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                                    if (WithExceptions)
                                    {
                                        throw new Exception(ex.Message);
                                    }
                                    return false;
                                }
                                else
                                {
                                    System.Threading.Thread.Sleep(1000);
                                    if (_DirectSQLConnection.State == ConnectionState.Closed)
                                        _DirectSQLConnection.Open();
                                    continue;
                                }
                            }
                        }
                        //}
                    }
                    else
                    {
                        throw new Exception("Неправильное имя базы данных");
                        //return false;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Получить одиночное значение по запросу.
        /// </summary>
        /// <param name="strSQL">Сам скрипт запроса</param>
        /// <param name="DefaultValue">Значение по умолчанию, если что то не так с запросом или значение NULL</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в лог файл</param>
        /// <returns>Одно значение от запроса. Первое поле с первой строки.</returns>
        public int GetScalarValue(string strSQL, int DefaultValue, eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            string strValue = GetScalarValue(strSQL, DefaultValue.ToString(), WriteToLog);
            if (strValue != "")
                return Convert.ToInt32(strValue);
            else
                return DefaultValue;
        }

        /// <summary>
        /// Получить одиночное значение по запросу.
        /// </summary>
        /// <param name="strSQL">Сам скрипт запроса</param>
        /// <param name="DefaultValue">Значение по умолчанию, если что то не так с запросом или значение NULL</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в лог файл</param>
        /// <returns>Одно значение от запроса. Первое поле с первой строки.</returns>
        public string GetScalarValue(string strSQL, string DefaultValue = "", eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            string tmpValue = null;
            SqlConnection _SqlConnection = null;
            SqlCommand objSQLCommand = new SqlCommand();
            object objTemp = DBNull.Value;
            tmpValue = DefaultValue;
            bool blnExitExecute = false;
            bool blnReConnect = false;
            int CounterRetries = 0;
            if (!Connected)
                blnReConnect = true;
            
            do
            {
                try
                {
                    ConnectToSQL(ReConnect: blnReConnect);

                    lock (_DirectSQLConnection)
                    {
                        long StartTicks = System.DateTime.Now.Ticks;

                        objSQLCommand.CommandText = strSQL;
                        if (_DirectSQLConnection.State == ConnectionState.Open)
                            objSQLCommand.Connection = _DirectSQLConnection;
                        else
                        {
                            _SqlConnection = new SqlConnection(SQLConnectionString);
                            _SqlConnection.Open();
                            objSQLCommand.Connection = _SqlConnection;
                        }
                        objSQLCommand.CommandTimeout = 0;
                        objTemp = objSQLCommand.ExecuteScalar();

                        //_DirectSQLConnection.State == ConnectionState.
                        CalculateExecutionTime(StartTicks, strSQL);
                    }
                    blnExitExecute = true;
                }
                catch (SqlException ex)
                {
                        blnReConnect = true; //делаем реконнект
                        _DirectSQLConnection.Close();
                        if (CountRetry == -1)
                        {
                            blnExitExecute = false;
                        }
                        else if (CounterRetries < CountRetry)
                        {
                            blnExitExecute = false;
                            CounterRetries += 1;
                        }
                }
            } while (!(blnExitExecute));

            if (objTemp != DBNull.Value && objTemp != null)
            {
                tmpValue = Convert.ToString(objTemp);
            }
            if ((LogSqlScripts == Static_Class.eBoolean.Yes & WriteToLog == eWriteToLog.ByDefault) | WriteToLog == eWriteToLog.AlwaysWrite)
            {
                _Logs.WriteToLog("ExecuteScalar: " + strSQL + " Found:" + tmpValue, _LogSqlScriptsNumber);
            }
            objSQLCommand.Dispose();
            
            if (_SqlConnection != null)
            {
                _SqlConnection.Close();
                _SqlConnection = null;
            }
            return tmpValue;
        }

        /// <summary>
        /// Получить DataTable по запросу к базе.
        /// </summary>
        /// <param name="strSQL">Сам скрипт запроса</param>
        /// <param name="strPrefixLog">Префикс при записи в лог файл. Не влияет на получение DataTable</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в лог файл</param>
        /// <returns>Возвращает DataTable. Если DataTable is null то произошла ошибка в скрипте.</returns>
        public DataTable GetDataTable(string strSQL, string strPrefixLog = "GetDateTable: ", eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            DataTable dtReturn = null;
            SqlConnection _SqlConnection = null;
            SqlCommand objSQLCommand = new SqlCommand();
            SqlDataReader dReader = null;
            bool blnExitExecute = false;
            bool blnReConnect = false;
            int CounterRetries = 0;

            if (string.IsNullOrEmpty(SQLConnectionString) || strSQL.Length == 0)
            {
                return null;
            }

            if (!Connected)
                blnReConnect = true;

            do
            {
                try
                {
                    ConnectToSQL(ReConnect: blnReConnect);
                    lock (_DirectSQLConnection)
                    {
                        objSQLCommand.CommandText = strSQL;
                        if (_DirectSQLConnection.State == ConnectionState.Open)
                            objSQLCommand.Connection = _DirectSQLConnection;
                        else
                        {
                            _SqlConnection = new SqlConnection(SQLConnectionString);
                            _SqlConnection.Open();
                            objSQLCommand.Connection = _SqlConnection;
                        }
                        objSQLCommand.CommandTimeout = 0;
                        long StartTicks = System.DateTime.Now.Ticks;

                        if (RunAsynchronous)
                        {
                            #region RunAsynchronous
                            _AsyncOperation = eAsyncOperations.Running;

                            IAsyncResult objResult = objSQLCommand.BeginExecuteReader();
                            while (_AsyncOperation == eAsyncOperations.DoCancel || !objResult.IsCompleted)
                            {
                                Application.DoEvents();
                            }
                            if (_AsyncOperation == eAsyncOperations.DoCancel)
                            {
                                objSQLCommand.Cancel();
                                _AsyncOperation = eAsyncOperations.DoNothing;
                                return null;
                            }
                            dReader = objSQLCommand.EndExecuteReader(objResult);
                            #endregion
                        }
                        else
                        {
                            dReader = objSQLCommand.ExecuteReader();
                        }
                        dtReturn = new DataTable();
                        dtReturn.Load(dReader);
                        CalculateExecutionTime(StartTicks, strSQL);
                        _AsyncOperation = eAsyncOperations.DoNothing;

                        dtReturn.TableName = GetTableName(strSQL);
                        dReader.Close();
                    }
                    blnExitExecute = true;
                }
                catch (SqlException ex)
                {
                    _Logs.WriteToLog(ex, "Retry execute after SQL Exception ...", strSQL, _LogSqlScriptsNumber);
                    blnReConnect = true; //делаем реконнект
                    _DirectSQLConnection.Close();
                    if (CountRetry == -1)
                    {
                        blnExitExecute = false;
                    }
                    else if (CounterRetries < CountRetry)
                    {
                        blnExitExecute = false;
                        CounterRetries += 1;
                    }

                    if (!blnExitExecute)
                    {
                        if (dReader != null && !dReader.IsClosed)
                            dReader.Close();
                    }
                }
            } while (!(blnExitExecute));

            if ((LogSqlScripts == Static_Class.eBoolean.Yes & WriteToLog == eWriteToLog.ByDefault) | WriteToLog == eWriteToLog.AlwaysWrite)
            {
                _Logs.WriteToLog(strPrefixLog + strSQL + " Records:" + dtReturn.Rows.Count, _LogSqlScriptsNumber);
            }
            objSQLCommand.Dispose();

            if (_SqlConnection != null)
            {
                _SqlConnection.Close();
                _SqlConnection = null;
            }
            return dtReturn;
        }

        /// <summary>
        /// Поличить DataRow по запросу к базе.
        /// </summary>
        /// <param name="strSQL">Сам скрипт запроса</param>
        /// <param name="strPrefixLog">Префикс при записи в лог файл. Не влияет на получение DataRow</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в лог файл</param>
        /// <returns>Возвращает DataRow. Если DataRow is null то произошла ошибка в скрипте.</returns>
        public DataRow GetDataRow(string strSQL, eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            DataRow objRow = null;
            DataTable objDataTable = GetDataTable(strSQL, "GetDataRow: ", WriteToLog);
            if (objDataTable != null)
            {
                if (objDataTable.Rows.Count > 0)
                {
                    objRow = objDataTable.Rows[0];
                }
            }
            return objRow;
        }
       
        /// <summary>
        /// Выполнение скрипта на SQL Сервере.
        /// </summary>
        /// <param name="strSQL">Сам скрипт запроса</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в логфайл</param>
        /// <returns>Возвращает RowAffected. Если скрипт с ошибкой то возвращает -1.</returns>
        public int ExecSQLDirect(string strSQL, eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            //CountRetry:
            // 0 - disable
            // -1 - unlimit
            bool blnExitExecute = false;
            bool blnReConnect = false;
            int CounterRetries = 0;
            int intRowsAffected = -1;
            SqlCommand objSQLCommand;
            IAsyncResult objResult;
            long StartTicks;

            if (!Connected)
                blnReConnect = true;
            
            try
            {
                objSQLCommand = new SqlCommand(strSQL, _DirectSQLConnection);
                do
                {
                    ConnectToSQL(ReConnect: blnReConnect);
                    if ((LogSqlScripts == Static_Class.eBoolean.Yes & WriteToLog == eWriteToLog.ByDefault) | WriteToLog == eWriteToLog.AlwaysWrite)
                    {
                        _Logs.WriteToLog("ExecSQLDirect: " + objSQLCommand.CommandText, _LogSqlScriptsNumber);
                    }

                    blnExitExecute = true;

                    try
                    {
                        lock (_DirectSQLConnection)
                        {
                            objSQLCommand.CommandTimeout = 0;
                            //intRowsAffected = myCommand.ExecuteNonQuery()
                            StartTicks = System.DateTime.Now.Ticks;
                            if (RunAsynchronous)
                            {
                                _AsyncOperation = eAsyncOperations.Running;
                                objResult = objSQLCommand.BeginExecuteNonQuery();
                                while (_AsyncOperation == eAsyncOperations.DoCancel || !objResult.IsCompleted)
                                {
                                    Application.DoEvents();
                                }
                                if (_AsyncOperation == eAsyncOperations.DoCancel)
                                {
                                    objSQLCommand.Cancel();
                                    _AsyncOperation = eAsyncOperations.DoNothing;
                                    return -1;
                                }
                                intRowsAffected = objSQLCommand.EndExecuteNonQuery(objResult);
                            }
                            else
                            {
                                intRowsAffected = objSQLCommand.ExecuteNonQuery();
                            }

                            CalculateExecutionTime(StartTicks, strSQL);
                            _AsyncOperation = eAsyncOperations.DoNothing;
                        }
                    }
                    catch (SqlException ex)
                    {
                        blnReConnect = true; //делаем реконнект
                        _DirectSQLConnection.Close();
                        if (CountRetry == -1)
                        {
                            blnExitExecute = false;
                        }
                        else if (CounterRetries < CountRetry)
                        {
                            blnExitExecute = false;
                            CounterRetries += 1;
                        }
                    }
                } while (!(blnExitExecute));

                objSQLCommand.Dispose();
            }
            catch (Exception ex)
            {
                _Logs.WriteToLog("ExecSQLDirect.Execute "
                    + "\r\n" + ex.Message
                    + "\r\n" + ex.StackTrace
                    + "\r\n" + strSQL, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message + " - \r\n" + strSQL, ex);
                }
            }

            return intRowsAffected;
        }

        private SemaphoreSlim _processingSemaphore = new SemaphoreSlim(20);
        /// <summary>
        /// Выполнение SQL команды на SQL Сервере.
        /// </summary>
        /// <param name="objSQLCommand">SQL команда для выполнения на сервере</param>
        /// <param name="WriteToLog">Параметер отвечающий за запись скрипта в логфайл</param>
        /// <returns>Возвращает RowAffected. Если скрипт с ошибкой то возвращает -1.</returns>
        public async void ExecSQLDirectAsync(string strSQL, eWriteToLog WriteToLog = eWriteToLog.ByDefault)
        {
            SqlConnection localConnection = new SqlConnection(SQLConnectionString);
            SqlCommand objSQLCommand = new SqlCommand(strSQL, localConnection);
            long StartTicks;

            _processingSemaphore.Wait();
            try
            {
                localConnection.Open();
                if ((LogSqlScripts == Static_Class.eBoolean.Yes & WriteToLog == eWriteToLog.ByDefault) | WriteToLog == eWriteToLog.AlwaysWrite)
                {
                    _Logs.WriteToLog("ExecSQLDirectAsync: " + objSQLCommand.CommandText, _LogSqlScriptsNumber);
                }

                StartTicks = System.DateTime.Now.Ticks;
                await objSQLCommand.ExecuteNonQueryAsync();
                CalculateExecutionTime(StartTicks, objSQLCommand.CommandText);

            }
            catch (Exception ex)
            {
                _Logs.WriteToLog("ExecSQLDirect.Execute "
                    + "\r\n" + ex.Message
                    + "\r\n" + ex.StackTrace
                    + "\r\n" + objSQLCommand.CommandText, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message + " - \r\n" + objSQLCommand.CommandText, ex);
                }
            }
            finally
            {
                objSQLCommand.Dispose();
                _processingSemaphore.Release();
            }
        }

        /// <summary>
        /// Заполняет ComboBox по SQL запросу. Причем если поля в запросе 2 тогда первое поле идет как ValueMember а второе как DisplayMember.
        /// Возможно добавить одну строку и значение в начало.
        /// </summary>
        /// <param name="strSQL">Сам запрос</param>
        /// <param name="objComboBox">ComboBox который заполняем</param>
        /// <param name="NonePresent">Если нужно добавить строку в начало</param>
        /// <param name="NoneValue">Значение строки</param>
        /// <param name="NoneText">Текст строки</param>
        public void LoadCombo(string strSQL, ComboBox objComboBox, bool NonePresent = false, object NoneValue = null, string NoneText = null)
        {
            System.Data.DataTable xData = null;
            DataView xView = null;
            int oldValue = -1;
            try
            {
                if (NoneText == null)
                {
                    NoneText = conNone;
                }

                if (objComboBox.SelectedValue != null)
                {
                    oldValue = Convert.ToInt32(objComboBox.SelectedValue);
                }
                xData = GetDataTable(strSQL);

                if (NonePresent)
                {
                    DataRow objRow = xData.NewRow();
                    objRow[0] = NoneValue;
                    objRow[1] = NoneText;
                    xData.Columns[0].AllowDBNull = true;
                    xData.Rows.InsertAt(objRow, 0);
                }

                xView = xData.DefaultView;

                objComboBox.BeginUpdate();
                objComboBox.DataSource = xView;
                objComboBox.ValueMember = xData.Columns[0].ColumnName;
                if (xData.Columns.Count > 1)
                    objComboBox.DisplayMember = xData.Columns[1].ColumnName;
                else
                    objComboBox.DisplayMember = xData.Columns[0].ColumnName;
                objComboBox.Enabled = !(xData.Rows.Count == 0 && objComboBox.DropDownStyle == ComboBoxStyle.DropDownList);
                objComboBox.EndUpdate();
                if ((xData.Rows.Count > 0))
                {
                    if (oldValue > -1)
                    {
                        objComboBox.SelectedValue = oldValue;
                    }
                    else if (objComboBox.Tag != null)
                    {
                        objComboBox.SelectedValue = objComboBox.Tag;
                    }
                }

            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
        }
        /// <summary>
        /// Заполняет ListBox по SQL запросу. 
        /// Причем если поля в запросе 2 тогда нулевое поле идет как ValueMember, а первое как DisplayMember.
        /// Если поле одно, тогда и ValueMember и DisplayMember то же самое поле.
        /// </summary>
        /// <param name="strSQL">Запрос к базе</param>
        /// <param name="objListBox">Сам объект заполнения</param>
        public void LoadListBox(string strSQL, ListBox objListBox)
        {
            System.Data.DataTable xData;
            try
            {
                xData = GetDataTable(strSQL);
                LoadListBox(xData, objListBox);
            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Заполняет ListBox из DataTable. 
        /// Причем если поля в запросе 2 тогда нулевое поле идет как ValueMember, а первое как DisplayMember.
        /// Если поле одно, тогда и ValueMember и DisplayMember то же самое поле.
        /// </summary>
        /// <param name="xData">Сама таблица</param>
        /// <param name="objListBox">Сам объект заполнения</param>
        public void LoadListBox(System.Data.DataTable xData, ListBox objListBox)
        {
            DataView xView;

            try
            {
                xView = xData.DefaultView;
                objListBox.DataSource = xView;
                if (xData.Columns.Count > 1)
                    objListBox.DisplayMember = xData.Columns[1].ColumnName;
                else
                    objListBox.DisplayMember = xData.Columns[0].ColumnName;
                objListBox.ValueMember = xData.Columns[0].ColumnName;

            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        public void LoadCheckedListBox(System.Data.DataTable xData, ref CheckedListBox objCheckedListBox)
        {
            DataView xView = null;
            //Did not finish
            try
            {
                xView = xData.DefaultView;
                var _with4 = objCheckedListBox;
                _with4.DataSource = xView;
                _with4.DisplayMember = xData.Columns[1].ColumnName;
                _with4.ValueMember = xData.Columns[0].ColumnName;

            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

        /// <summary>
        /// Определяет имя Host на котором запущенно "приложение" на текущей базе данных
        /// </summary>
        /// <param name="strApplicationName">Искомое приложение</param>
        /// <returns>имя хоста</returns>
        public string GetHostNameViaApplicationName(string strApplicationName)
        {
            string strSQL = "EXEC sp_who2";
            string strReturn = "";
            int LenApplicationName = strApplicationName.Length;
            try
            {
                if (LenApplicationName > 0)
                {
                    DataTable _DataTable = GetDataTable(strSQL);
                    foreach (DataRow _DataRow in _DataTable.Rows)
                    {
                        if (!Convert.IsDBNull(_DataRow["dbname"]))
                        {
                            if (_DataRow["dbname"].ToString().ToLower() == SQLDatabase.ToLower())
                            {
                                if (_DataRow["ProgramName"].ToString().Substring(0, LenApplicationName).ToLower() == strApplicationName.ToLower())
                                {
                                    strReturn = _DataRow["HostName"].ToString().Trim();
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
            return strReturn;
        }

        /// <summary>
        /// Задает новое уникальное значение поля.
        /// К примеру на входе "Folder" и в таблице уже присутствуют записи с "Folder1" и "Folder2". Тогда будет заданно значение "Folder3"
        /// </summary>
        /// <param name="strName">Исходный текст поля</param>
        /// <param name="strFieldName">Поле в таблице</param>
        /// <param name="strTable">Таблица в базе</param>
        /// <returns></returns>
        public string NewPrimaryFieldNumerator(string strName, string strFieldName, string strTable)
        {
            int intTmp;
            string strSQL = null;
            try
            {
                do
                {
                    strName = Static_Class.NewNameNumerator(strName);
                    strSQL = "SELECT Count(*) FROM " + strTable + " WHERE " + strFieldName + " = N'" + strName + "'";
                    intTmp = GetScalarValue(strSQL, 0);
                } while (!(intTmp == 0));
            }
            catch (Exception ex)
            {
                _Logs.WriteToLog(ex, _LogSqlScriptsNumber);
                if (WithExceptions)
                {
                    throw new Exception(ex.Message);
                }
            }
            return strName;
        }

    }
}
