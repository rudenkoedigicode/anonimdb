﻿using System;
using StandartClassLibrary.ConnectDB;
using StandartClassLibrary.Logs;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AnonimDB
{
    public partial class MainForm : Form
    {
        private Logs_Class glbLogs = new Logs_Class();
        public MainForm()
        {
            InitializeComponent();
            glbLogs.Open("Info", "Error", "SQL");
        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (textBoxServer.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Server can not be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxServer.Focus();
                return;
            }
            if (textBoxLogin.Text.IsNullOrEmpty())
            {
                MessageBox.Show("Login can not be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxLogin.Focus();
                return;
            }
            SqlConnectionStringBuilder scb = new SqlConnectionStringBuilder();
            //scb.Authentication = SqlAuthenticationMethod.SqlPassword;
            scb.TrustServerCertificate = true;
            scb.DataSource = textBoxServer.Text;
            if (!chkWindowsAuthentication.Checked)
            {
                scb.UserID = textBoxLogin.Text;
                scb.Password = textBoxPassword.Text;
            }
            else
            {
                //Integrated Security = SSPI
                scb.IntegratedSecurity = true;
            }
            scb.InitialCatalog = "lms_active";

            SQL_Class mainConnect = new SQL_Class(glbLogs, 3, "AnnonimDB");
            mainConnect.WithExceptions = true;
            mainConnect.SQLConnectionString = scb.ConnectionString;
            try
            {
                if (!mainConnect.CheckSQLConnection(scb.ConnectionString))
                {
                    MessageBox.Show("Connection faild", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                textBoxLogin.Focus();
                return;
            }

            ProgressForm _form = new ProgressForm() { mainConnection = mainConnect };
            if (_form.ShowDialog() == DialogResult.OK)
                this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MainForm_Shown(object sender, EventArgs e)
        {
            if (Debugger.IsAttached)
            {
                textBoxServer.Text = "SVAROG\\SQLEXPRESS";
                textBoxLogin.Text = "sa";
                textBoxPassword.Text = "klassno";
            }
        }

        private void chkWindowsAuthentication_CheckedChanged(object sender, EventArgs e)
        {
            textBoxLogin.Enabled = !chkWindowsAuthentication.Checked;
            textBoxPassword.Enabled = !chkWindowsAuthentication.Checked;
        }
    }
}
