﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using StandartClassLibrary.ConnectDB;
using System.Linq;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using ICSharpCode.SharpZipLib.Zip;

namespace AnonimDB
{
    public partial class ProgressForm : Form
    {
        public SQL_Class mainConnection { get; set; }
        private Dictionary<string, int> UniqueName = new Dictionary<string, int>();
        private SemaphoreSlim _processingSemaphore = new SemaphoreSlim(20);

        FieldTables[] arrFields = new FieldTables[] {
            new FieldTables("BRNAME", new List<string>() { "BRANCH" }) { UniqueIndexName = "NAME" },
            new FieldTables("STREET1", new List<string>() { "BRANCH" }) { UniqueIndexName = "STREET" },
            new FieldTables("STREET2", new List<string>() { "BRANCH" }) { UniqueIndexName = "STREET" },
            new FieldTables("NONAME1", new List<string>() { "BRANCH", "DEALER", "LESSOR", "TRANSACT" }) { UniqueIndexName = "NAME" },
            new FieldTables("NONAME2", new List<string>() { "BRANCH", "DEALER", "LESSOR", "TRANSACT" }) { UniqueIndexName = "NAME" },
            new FieldTables("NOSTREET", new List<string>() { "BRANCH", "DEALER", "LESSOR", "TRANSACT" }) { UniqueIndexName = "STREET" },
            //new FieldTables("ZIP", new List<string>() { "BRANCH" }) { PrefixValue = "555" },
            new FieldTables("NITEPHONE", new List<string>() { "BRANCH" }) { PrefixValue = "1111" },
            new FieldTables("DAYPHONE", new List<string>() { "BRANCH" }) { PrefixValue = "1111" },
            new FieldTables("LESNAME", new List<string>() { "BRANCH", "DEALTRAD", "FUNDDEAL", "LESSOR" }),
            new FieldTables("DENAME", new List<string>() { "DEALER" }) { UniqueIndexName = "NAME" },
            //new FieldTables("DESNAME", new List<string>() { "DEALER", "DEALTRAD", "FUNDDEAL", "TRANSACT" }) { UniqueIndexName = "DN" },
            new FieldTables("MSTREET1", new List<string>() { "DEALER", "LESSOR" }) { UniqueIndexName = "STREET" },
            new FieldTables("MSTREET2", new List<string>() { "DEALER", "LESSOR" }) { UniqueIndexName = "STREET" },
            //new FieldTables("MZIP", new List<string>() { "DEALER" }) { PrefixValue = "555" },
            new FieldTables("C1NAME", new List<string>() { "DEALER", "LESSOR" }) { UniqueIndexName = "NAME" },
            new FieldTables("C1PHONE", new List<string>() { "DEALER", "LESSOR" }) { PrefixValue = "1111" },
            new FieldTables("C2NAME", new List<string>() { "DEALER", "LESSOR" }) { UniqueIndexName = "NAME" },
            new FieldTables("C2PHONE", new List<string>() { "DEALER", "LESSOR" }) { PrefixValue = "1111" },
            new FieldTables("C3NAME", new List<string>() { "DEALER", "LESSOR" }) { UniqueIndexName = "NAME" },
            new FieldTables("C3PHONE", new List<string>() { "DEALER", "LESSOR" }) { PrefixValue = "1111" },
            //new FieldTables("NOZIP", new List<string>() { "DEALER", "LESSOR", "TRANSACT" }) { PrefixValue = "555" },
            new FieldTables("LENAME", new List<string>() { "LESSOR" }) { UniqueIndexName = "NAME" },
            new FieldTables("LHNAME", new List<string>() { "LESSOR", "TRANSACT" }) { UniqueIndexName = "NAME" },
            new FieldTables("LHSTREET", new List<string>() { "LESSOR", "TRANSACT" }) { UniqueIndexName = "STREET" },
            //new FieldTables("LHZIP", new List<string>() { "LESSOR", "TRANSACT" }) { PrefixValue = "555" },
            new FieldTables("LESSEE", new List<string>() { "TRANSACT" }) { UniqueIndexName = "NAME" },
            new FieldTables("LESTREET", new List<string>() { "TRANSACT" }) { UniqueIndexName = "STREET" },
            //new FieldTables("LEZIP", new List<string>() { "TRANSACT" }) { PrefixValue = "555" },
            new FieldTables("PONAME", new List<string>() { "TRANSACT" }) { UniqueIndexName = "NAME" },
            new FieldTables("POSTREET", new List<string>() { "TRANSACT" }) { UniqueIndexName = "STREET" },
            //new FieldTables("POZIP", new List<string>() { "TRANSACT" }) { PrefixValue = "555" }
            new FieldTables("VIN", new List<string>() { "POOLALLOCERROR", "POOLXFERLOAD", "POOLXFERERROR", "POOLALLOCLOAD", "EXPIREWORK", "POOLDED", "MERCEDESFIX", "IMPORTERROR" }) { PrefixValue = "1111" },
            new FieldTables("NEWVIN", new List<string>() { "TRANSACT" }) { PrefixValue = "1111" },
        };

        #region Tables
        //TableFields[] arrTables = new TableFields[] {
        //    new TableFields("BRANCH", new List<TableField>() {
        //            new TableField("BRNAME"),
        //            new TableField("STREET1"),
        //            new TableField("STREET2"),
        //            new TableField("NONAME1"),
        //            new TableField("NONAME2"),
        //            new TableField("NOSTREET"),
        //            new TableField("ZIP", "55"),
        //            new TableField("NITEPHONE", "55"),
        //            new TableField("DAYPHONE", "55"),
        //            new TableField("LESNAME")
        //        }),
        //    new TableFields("DEALER", new List<TableField>() {
        //            new TableField("DENAME"),
        //            new TableField("DESNAME"),
        //            new TableField("MSTREET1"),
        //            new TableField("MSTREET2"),
        //            new TableField("MZIP", "55"),
        //            new TableField("C1NAME"),
        //            new TableField("C1PHONE", "55"),
        //            new TableField("C2NAME"),
        //            new TableField("C2PHONE", "55"),
        //            new TableField("C3NAME"),
        //            new TableField("C3PHONE", "55"),
        //            new TableField("NONAME1"),
        //            new TableField("NONAME2"),
        //            new TableField("NOSTREET"),
        //            new TableField("NOZIP", "55")
        //        }),
        //        new TableFields("DEALTRAD", new List<TableField>() {
        //            new TableField("DESNAME"),
        //            new TableField("LESNAME")
        //        }),
        //        new TableFields("FUNDDEAL", new List<TableField>() {
        //            new TableField("DESNAME"),
        //            new TableField("LESNAME")
        //        }),
        //        new TableFields("LESSOR", new List<TableField>() { 
        //            new TableField("LENAME"),
        //            new TableField("LESNAME"),
        //            new TableField("MSTREET1"),
        //            new TableField("MSTREET2"),
        //            new TableField("NOZIP", "55"),
        //            new TableField("LHSNAME"),
        //            new TableField("LHTREET"),
        //            new TableField("LHZIP", "55"),
        //            new TableField("C1NAME"),
        //            new TableField("C1PHONE", "55"),
        //            new TableField("C2NAME"),
        //            new TableField("C2PHONE", "55"),
        //            new TableField("C3NAME"),
        //            new TableField("C3PHONE", "55"),
        //            new TableField("NONAME1"),
        //            new TableField("NONAME2"),
        //            new TableField("NOSTREET"),
        //            new TableField("NOZIP", "55")
        //        }),
        //        new TableFields("TRANSACT", new List<TableField>() {
        //            new TableField("DESNAME"),
        //            new TableField("LESSEE"),
        //            new TableField("LESTREET"),
        //            new TableField("LEZIP", "55"),
        //            new TableField("PONAME"),
        //            new TableField("POSTREET"),
        //            new TableField("POZIP", "55"),
        //            new TableField("NONAME1"),
        //            new TableField("NONAME2"),
        //            new TableField("NOSTREET"),
        //            new TableField("NOZIP", "55"),
        //            new TableField("LHNAME"),
        //            new TableField("LHSTREET"),
        //            new TableField("LHZIP", "55")
        //        }),
        //        new TableFields("TREASURY", new List<TableField>() {
        //            new TableField("LESNAME")
        //        })
        //    };
        #endregion

        public ProgressForm()
        {
            InitializeComponent();
            progBarMain.Maximum = arrFields.Length + 2;
        }

        private void bwMain_DoWork(object sender, DoWorkEventArgs e)
        {
            string strSQL;
            int progBarM = 1;
            int progBarS = 0;

            #region Stage 1
            bwMain.ReportProgress(1, new UserStateClass("Create copy DB", progBarM, 0));
            // If copy Database exists when delete it
            strSQL = $"select count(*) FROM master.sys.databases WHERE Name = '{StaticClass.anonimDataBaseName}'";
            if (mainConnection.GetScalarValue(strSQL, 0) > 0)
            {
                strSQL = $"DROP DATABASE {StaticClass.anonimDataBaseName}";
                mainConnection.ExecSQLDirect(strSQL);
            }
            // Get backup folder
            bwMain.ReportProgress(1, new UserStateClass("Get backup folder", progBarM, 25));
            if (bwMain.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            string backupFolderName = @"C:\Tmp\";
            if (!Directory.Exists(backupFolderName))
                Directory.CreateDirectory(backupFolderName);

            // Get DB folder
            bwMain.ReportProgress(1, new UserStateClass("Get DB folder", progBarM, 50));
            strSQL = @"SELECT smf.physical_name
FROM master.sys.databases sd
INNER JOIN master.sys.master_files smf ON sd.database_id = smf.database_id
WHERE sd.name = '" + StaticClass.dataBaseName + "'";
            DataTable dtDatabase = mainConnection.GetDataTable(strSQL);
            string databaseFolder = dtDatabase.Rows[0][0].ToString();
            FileInfo fi = new FileInfo(databaseFolder);
            databaseFolder = fi.DirectoryName;
            bwMain.ReportProgress(1, new UserStateClass("Backup", progBarM, 75));
            if (bwMain.CancellationPending)
            {
                e.Cancel = true;
                return;
            }

            //1. Backup
            string backupFile = backupFolderName + StaticClass.dataBaseName + ".bak";

            if (File.Exists(backupFile))
                File.Delete(backupFile);

            strSQL = $"BACKUP DATABASE {StaticClass.dataBaseName} TO DISK = '{backupFile}'";
            mainConnection.ExecSQLDirect(strSQL);
            //2. Restore
            bwMain.ReportProgress(1, new UserStateClass("Restore", progBarM, 99));
            strSQL = $"RESTORE DATABASE {StaticClass.anonimDataBaseName} FROM DISK = '{backupFile}' WITH FILE = 1, " +
                $"MOVE N'{StaticClass.dataBaseName}_dat' TO '{databaseFolder}\\{StaticClass.anonimDataBaseName}.mdf', " +
                $"MOVE N'{StaticClass.dataBaseName}_log' TO '{databaseFolder}\\{StaticClass.anonimDataBaseName}_log.ldf', " +
                "KEEP_REPLICATION,  NOUNLOAD,  REPLACE,  STATS = 5";
            mainConnection.ExecSQLDirect(strSQL);

            File.Delete(backupFile);
            if (bwMain.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            #endregion
            #region Stage 2 Update
            mainConnection.ChangeDatabase(StaticClass.anonimDataBaseName);
            bwMain.ReportProgress(2, new UserStateClass("Update tables", progBarM++, 0));
            foreach (FieldTables _item in arrFields)
            {
                progBarS = 0;
                if (!_item.PrefixValue.IsNullOrEmpty())
                {
                    Invoke((MethodInvoker)delegate
                    {
                        progBarSecond.Maximum = _item.TableNames.Count;
                    });
                    foreach (string _tableName in _item.TableNames)
                    {
                        bwMain.ReportProgress(2, new UserStateClass(_item.FieldName, progBarM, progBarS++));
                        strSQL = $"UPDATE {_tableName} SET {_item.FieldName} = '{_item.PrefixValue}' + SUBSTRING({_item.FieldName}, {_item.PrefixValue.Length + 1}, 20)" +
                            $" WHERE {_item.FieldName} IS NOT NULL";
                        mainConnection.ExecSQLDirect(strSQL);
                        if (bwMain.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                    }
                }
                else
                {
                    int IndexName = 1;
                    if (UniqueName.ContainsKey(_item.UniqueIndexName))
                        IndexName = UniqueName[_item.UniqueIndexName];
                    progBarS = 1;

                    bwMain.ReportProgress(2, new UserStateClass(_item.FieldName, progBarM, progBarS++));
                    List<string> arrSQL = new List<string>();
                    _item.TableNames.ForEach(_tableName => arrSQL.Add($"SELECT DISTINCT {_item.FieldName} FROM {_tableName} WHERE {_item.FieldName} IS NOT NULL"));
                    strSQL = string.Join(" Union all ", arrSQL);
                    DataTable dtUniqueValues = mainConnection.GetDataTable(strSQL);
                    if (dtUniqueValues.Rows.Count > 0)
                    {
                        Invoke((MethodInvoker)delegate
                        {
                            progBarSecond.Maximum = dtUniqueValues.Rows.Count + 1;
                            //progBarSecond.Maximum = dtUniqueValues.Rows.Count * _item.TableNames.Count + 1;
                        });
                        string indexFormat = StaticClass.GetFormat(dtUniqueValues.Rows.Count);
                        foreach (DataRow _row in dtUniqueValues.Rows)
                        {
                            string _newValue = _item.UniqueIndexName + " " + IndexName.ToString(indexFormat);
                            IndexName++;
                            bwMain.ReportProgress(2, new UserStateClass(_item.FieldName, progBarM, progBarS++));
                            foreach (string _tableName in _item.TableNames)
                            {
                                string SQLquery = $"UPDATE {_tableName} SET {_item.FieldName} = '{_newValue}' WHERE {_item.FieldName} = '{_row[0].ToString()}'";
                                mainConnection.ExecSQLDirect(SQLquery);
                                //_processingSemaphore.Wait();
                                //mainConnection.ExecSQLDirectAsync(SQLquery);
                                //_processingSemaphore.Release();
                                if (bwMain.CancellationPending)
                                {
                                    e.Cancel = true;
                                    return;
                                }
                            }
                        }

                        if (!UniqueName.ContainsKey(_item.UniqueIndexName))
                            UniqueName.Add(_item.UniqueIndexName, IndexName);
                    }
                }
                progBarM++;
            }
            #endregion
            #region Stage 3
            Invoke((MethodInvoker)delegate
            {
                progBarSecond.Maximum = 100;
            });
            bwMain.ReportProgress(3, new UserStateClass("Backup DB", arrFields.Length + 1, 50));
            backupFile = backupFolderName + StaticClass.anonimDataBaseName + ".bak";
            string zipFile = backupFolderName + StaticClass.anonimDataBaseName + ".zip";
            if (File.Exists(backupFile))
                File.Delete(backupFile);
            if (bwMain.CancellationPending)
            {
                e.Cancel = true;
                return;
            }
            strSQL = $"BACKUP DATABASE {StaticClass.anonimDataBaseName} TO DISK = '{backupFile}'";
            mainConnection.ExecSQLDirect(strSQL);

            bwMain.ReportProgress(3, new UserStateClass("Backup DB zip", arrFields.Length + 1, 75));
            forZip.compressFile(backupFile, zipFile);

            bwMain.ReportProgress(4, new UserStateClass("Finish", arrFields.Length + 2, 100));
            #endregion
        }

        private void bwMain_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            UserStateClass _e = (UserStateClass)(e.UserState);
            labelStatus.Text = _e.MessageText;
            progBarMain.Value = _e.ProgBarMain;
            progBarSecond.Value = _e.ProgBarSecond;
            labelStage.Text = $"Stage: {e.ProgressPercentage}";
        }

        private void bwMain_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                MessageBox.Show("The proccess canceled", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                MessageBox.Show("The process completed successfully.", "Done", MessageBoxButtons.OK, MessageBoxIcon.Information);
                DialogResult = DialogResult.OK;
            }
            this.Close();
        }

        private void PrograssForm_Shown(object sender, EventArgs e)
        {
            bwMain.RunWorkerAsync();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            bwMain.CancelAsync();
        }
    }
}
