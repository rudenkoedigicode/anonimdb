﻿namespace AnonimDB
{
    partial class ProgressForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelStatus = new System.Windows.Forms.Label();
            this.progBarMain = new System.Windows.Forms.ProgressBar();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.bwMain = new System.ComponentModel.BackgroundWorker();
            this.progBarSecond = new System.Windows.Forms.ProgressBar();
            this.labelStage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelStatus
            // 
            this.labelStatus.Location = new System.Drawing.Point(12, 9);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(493, 22);
            this.labelStatus.TabIndex = 0;
            this.labelStatus.Text = "Doing...";
            // 
            // progBarMain
            // 
            this.progBarMain.Location = new System.Drawing.Point(3, 69);
            this.progBarMain.Name = "progBarMain";
            this.progBarMain.Size = new System.Drawing.Size(502, 23);
            this.progBarMain.TabIndex = 1;
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(430, 98);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "&Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // bwMain
            // 
            this.bwMain.WorkerReportsProgress = true;
            this.bwMain.WorkerSupportsCancellation = true;
            this.bwMain.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwMain_DoWork);
            this.bwMain.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bwMain_ProgressChanged);
            this.bwMain.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bwMain_RunWorkerCompleted);
            // 
            // progBarSecond
            // 
            this.progBarSecond.Location = new System.Drawing.Point(3, 40);
            this.progBarSecond.Name = "progBarSecond";
            this.progBarSecond.Size = new System.Drawing.Size(502, 23);
            this.progBarSecond.TabIndex = 1;
            // 
            // labelStage
            // 
            this.labelStage.Location = new System.Drawing.Point(12, 103);
            this.labelStage.Name = "labelStage";
            this.labelStage.Size = new System.Drawing.Size(104, 18);
            this.labelStage.TabIndex = 0;
            this.labelStage.Text = "Stage:";
            // 
            // ProgressForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(510, 129);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.progBarSecond);
            this.Controls.Add(this.progBarMain);
            this.Controls.Add(this.labelStage);
            this.Controls.Add(this.labelStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "ProgressForm";
            this.Text = "Prograss";
            this.Shown += new System.EventHandler(this.PrograssForm_Shown);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.ProgressBar progBarMain;
        private System.Windows.Forms.Button buttonCancel;
        private System.ComponentModel.BackgroundWorker bwMain;
        private System.Windows.Forms.ProgressBar progBarSecond;
        private System.Windows.Forms.Label labelStage;
    }
}