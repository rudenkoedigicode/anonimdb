﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimDB
{
    public static class StaticClass
    {
        public const string dataBaseName = "lms_active";
        public const string anonimDataBaseName = "lms_active_anon";
        public static bool IsNullOrEmpty(this string Source)
        {
            return string.IsNullOrEmpty(Source);
        }

        public static string GetFormat(int MaxValue)
        {
            string returnValue = "";
            if (MaxValue > 9)
                returnValue += "0";
            if (MaxValue > 99)
                returnValue += "0";
            if (MaxValue > 999)
                returnValue += "0";
            if (MaxValue > 9999)
                returnValue += "0";
            returnValue += "#";
            return returnValue;
        }
    }
}
