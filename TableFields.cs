﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnonimDB
{
    public class TableFields
    {
        public string TableName { get; private set; }
        public List<TableField> FieldNames { get; private set; }

        public TableFields(string tableName, List<TableField> fields)
        {
            TableName = tableName;
            FieldNames = fields;
        }
    }
    public class FieldTables
    {
        public string FieldName { get; private set; }
        public List<string> TableNames { get; private set; }
        public string PrefixValue { get; set; }
        public string UniqueIndexName { get; set; }

        public FieldTables(string fieldName, List<string> tables)
        {
            FieldName = fieldName;
            TableNames = tables;
            UniqueIndexName = fieldName;
        }
    }

    public class TableField
    {
        public string FieldName;
        public string PrefixText;

        public TableField(string fieldName, string prefixText = "")
        {
            FieldName = fieldName;
            if (prefixText.IsNullOrEmpty())
            {
                PrefixText = fieldName;
            }
        }
    }
}
